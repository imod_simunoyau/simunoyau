from math import *
import numpy as np
import random as rd
import copy as cp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def sub3(a, b):
        """Returns the difference between 3-vectors a and b."""
        return (a[0] - b[0], a[1] - b[1], a[2] - b[2])

def dist3(a, b):
        """Returns the distance between point 3-vectors a and b."""
        return len3(sub3(a, b))

def len3(v):
        """Returns the length of 3-vector v."""
        return sqrt(v[0]**2 + v[1]**2 + v[2]**2)

iterations=100
kuhn=60
radius=1000
rejections=0
a=np.zeros([iterations,1,3])


x=[0]
y=[0]
z=[500]
history=[]
coordlist=[(x[-1],y[-1],z[-1])]

i=0
direction=rd.randint(0,5)
history.append(direction)

while i<iterations-1:
        direction=rd.randint(0,5)
        if direction==0:
                x.append(x[-1]+kuhn)
        elif direction==1:
                x.append(x[-1]-kuhn)
        else:
                x.append(x[-1])
        if direction==2:
                y.append(y[-1]+kuhn)
        elif direction==3:
                y.append(y[-1]-kuhn)
        else:
                y.append(y[-1])
        if direction==4:
                z.append(z[-1]+kuhn)
        elif direction==5:
                z.append(z[-1]-kuhn)
        else:
                z.append(z[-1])
        comcoor=(x[-1],y[-1],z[-1])
        d=dist3(comcoor,(0,0,0))
        coordlist.append(comcoor)
        i+=1
        print(comcoor,d)
        if comcoor not in coordlist and d<radius:
                #print(('iterations',i,'rejections',rejections))
                coordlist.append(comcoor)
                i+=1
        else:
                #print('oh poop',comcoor)
                x[-1]=x[-2]
                y[-1]=y[-2]
                z[-1]=z[-2]
                print(rejections)
                rejections+=1
                if rejections>iterations:
                        print('could not find a solution, breaking')
                        break
                        
##sphere coordinates
#draw sphere
u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
xs=np.cos(u)*np.sin(v)
ys=np.sin(u)*np.sin(v)
zs=np.cos(v)



fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.plot(x,y,z,color="g")
#ax.plot(coordlist[0],coordlist[1],coordlist[2])
ax.plot_wireframe(xs*radius, ys*radius, zs*radius, color="r")

ax.set_xlim3d([-10,10])
ax.set_xlabel('X')
ax.set_ylim3d([-10,10])
ax.set_ylabel('Y')
ax.set_zlim3d([-10,10])
ax.set_zlabel('Z')


ax.scatter(x[0],y[0],z[0],color="b",s=100)
ax.scatter(x[-1],y[-1],z[-1],color="g",s=100)

plt.show()
