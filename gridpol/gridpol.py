import numpy as np
import random as rd
import copy as cp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def sub3(a, b):
        """Returns the difference between 3-vectors a and b."""
        return (a[0] - b[0], a[1] - b[1], a[2] - b[2])

def dist3(a, b):
        """Returns the distance between point 3-vectors a and b."""
        return len3(sub3(a, b))

iterations=100
a=np.zeros([iterations,1,3])


x=[0]
y=[0]
z=[0]
history=[]

i=0
direction=rd.randint(0,5)
history.append(direction)

if direction==0:
	x.append(x[-1]+1)
elif direction==1:
	x.append(x[-1]-1)
else:
	x.append(x[-1])
if direction==2:
	y.append(y[-1]+1)
elif direction==3:
	y.append(y[-1]-1)
else:
	y.append(y[-1])
if direction==4:
	z.append(z[-1]+1)
elif direction==5:
	z.append(z[-1]-1)
else:
	z.append(z[-1])
	
while i<iterations:
	direction=rd.randint(0,5)
	if direction==0:
		if history[-1]!=1:
			x.append(x[-1]+1)
		else:
			x.append(x[-1])
	elif direction==1:
		if history[-1]!=0:
			x.append(x[-1]-1)
		else:
			x.append(x[-1])
	else:
		x.append(x[-1])
	if direction==2:
		if history[-1]!=3:
			y.append(y[-1]+1)
		else:
			y.append(y[-1])
	elif direction==3:
		if history[-1]!=2:
			y.append(y[-1]-1)
		else:
			y.append(y[-1])
	else:
		y.append(y[-1])
	if direction==4:
		if history[-1]!=5:
			z.append(z[-1]+1)
		else:
			z.append(z[-1])
	elif direction==5:
		if history[-1]!=4:
			z.append(z[-1]-1)
		else:
			z.append(z[-1])
	else:
		z.append(z[-1])
	i+=1


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
#ax=Axes3D(fig)
ax.plot(x,y,z)

plt.show()
