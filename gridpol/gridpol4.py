from math import *
import numpy as np
import random as rd
import copy as cp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fake error

def sub3(a, b):
        """Returns the difference between 3-vectors a and b."""
        return (a[0] - b[0], a[1] - b[1], a[2] - b[2])

def dist3(a, b):
        """Returns the distance between point 3-vectors a and b."""
        return len3(sub3(a, b))

def len3(v):
        """Returns the length of 3-vector v."""
        return sqrt(v[0]**2 + v[1]**2 + v[2]**2)

def confpoly(start_coord,iterations,kuhn,radius,coordlist):
        iterations=iterations
        kuhn=kuhn
        radius=radius
        rejections=0

        x=[]
        y=[]
        z=[]

        x.append(start_coord[0])
        y.append(start_coord[1])
        z.append(start_coord[2])
        
        history=[]
        coordlist=[(x[-1],y[-1],z[-1])]
        #gcoordlist=[]

        i=0
        direction=rd.randint(0,5)
        history.append(direction)

        while i<iterations-1:
                direction=rd.randint(0,5)
                if direction==0:
                        x.append(x[-1]+kuhn)
                elif direction==1:
                        x.append(x[-1]-kuhn)
                else:
                        x.append(x[-1])
                if direction==2:
                        y.append(y[-1]+kuhn)
                elif direction==3:
                        y.append(y[-1]-kuhn)
                else:
                        y.append(y[-1])
                if direction==4:
                        z.append(z[-1]+kuhn)
                elif direction==5:
                        z.append(z[-1]-kuhn)
                else:
                        z.append(z[-1])
                comcoor=(x[-1],y[-1],z[-1])
                d=dist3(comcoor,(0,0,0))
                #print(comcoor)
                if comcoor not in coordlist and d<radius:
                        #print(('iterations',i,'rejections',rejections))
                        coordlist.append(comcoor)
                        i+=1
                else:
                        x[-1]=x[-2]
                        y[-1]=y[-2]
                        z[-1]=z[-2]
                        rejections+=1
                        if rejections>iterations:
                                print('could not find a solution, breaking')
                                break
        cx=[]
        cy=[]
        cz=[]
        for elem in coordlist:
                cx.append(elem[0])
                cy.append(elem[1])
                cz.append(elem[2])
        return(coordlist,cx,cy,cz)

coordlist=[]
radius=1000

k1=confpoly((0,60,60),100,60,radius,coordlist)

coordlist.extend(k1[0])
print(len(coordlist))

k2=confpoly((0,0,60),100,60,radius,coordlist)

coordlist.extend(k2[0])
print(len(coordlist))

k3=confpoly((0,60,0),100,60,radius,coordlist)

x1=k1[1]
y1=k1[2]
z1=k1[3]

x2=k2[1]
y2=k2[2]
z2=k2[3]

x3=k3[1]
y3=k3[2]
z3=k3[3]

##check for duplicates
d = {}
for elem in coordlist:
    if elem in d:
        d[elem] += 1
    else:
        d[elem] = 1

print [x for x, y in d.items() if y > 1]

##sphere coordinates
##draw sphere
u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
xs=np.cos(u)*np.sin(v)
ys=np.sin(u)*np.sin(v)
zs=np.cos(v)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.plot_wireframe(xs*radius, ys*radius, zs*radius, color="r")

ax.plot(x1,y1,z1)
ax.plot(x2,y2,z2)
ax.plot(x3,y3,z3)

ax.set_xlim3d([-radius,radius])
ax.set_xlabel('X')
ax.set_ylim3d([-radius,radius])
ax.set_ylabel('Y')
ax.set_zlim3d([-radius,radius])
ax.set_zlabel('Z')

#ax.scatter(x[0],y[0],z[0],color="b",s=100)
#ax.scatter(x[-1],y[-1],z[-1],color="g",s=100)

plt.show()



##iterations=300
##kuhn=60
##radius=1000
##rejections=0
##a=np.zeros([iterations,1,3])
##
##
##x=[500]
##y=[500]
##z=[500]
##history=[]
##coordlist=[(x[-1],y[-1],z[-1])]
##gcoordlist=[]
##
##i=0
##direction=rd.randint(0,5)
##history.append(direction)
##
##while i<iterations-1:
##        direction=rd.randint(0,5)
##        if direction==0:
##                x.append(x[-1]+kuhn)
##        elif direction==1:
##                x.append(x[-1]-kuhn)
##        else:
##                x.append(x[-1])
##        if direction==2:
##                y.append(y[-1]+kuhn)
##        elif direction==3:
##                y.append(y[-1]-kuhn)
##        else:
##                y.append(y[-1])
##        if direction==4:
##                z.append(z[-1]+kuhn)
##        elif direction==5:
##                z.append(z[-1]-kuhn)
##        else:
##                z.append(z[-1])
##        comcoor=(x[-1],y[-1],z[-1])
##        d=dist3(comcoor,(0,0,0))
##        print(comcoor)
##        if comcoor not in coordlist and d<radius:
##                print(('iterations',i,'rejections',rejections))
##                coordlist.append(comcoor)
##                i+=1
##        else:
##                print('oh poop',comcoor)
##                x[-1]=x[-2]
##                y[-1]=y[-2]
##                z[-1]=z[-2]
##                rejections+=1
##                if rejections>iterations:
##                        print('could not find a solution, breaking')
##                        break
##                        
##

##sphere coordinates
#draw sphere
##u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
##xs=np.cos(u)*np.sin(v)
##ys=np.sin(u)*np.sin(v)
##zs=np.cos(v)
##
##fig = plt.figure()
##ax = fig.add_subplot(111, projection='3d')
##
##ax.plot_wireframe(xs*radius, ys*radius, zs*radius, color="r")
##ax.plot(x,y,z)
##ax.set_xlim3d([-radius,radius])
##ax.set_xlabel('X')
##ax.set_ylim3d([-radius,radius])
##ax.set_ylabel('Y')
##ax.set_zlim3d([-radius,radius])
##ax.set_zlabel('Z')
##
##ax.scatter(x[0],y[0],z[0],color="b",s=100)
##ax.scatter(x[-1],y[-1],z[-1],color="g",s=100)
##
##plt.show()
