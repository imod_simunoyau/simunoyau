import numpy as np
import random as rd
import copy as cp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def sub3(a, b):
        """Returns the difference between 3-vectors a and b."""
        return (a[0] - b[0], a[1] - b[1], a[2] - b[2])

def dist3(a, b):
        """Returns the distance between point 3-vectors a and b."""
        return len3(sub3(a, b))

iterations=30
rejections=0
a=np.zeros([iterations,1,3])


x=[0]
y=[0]
z=[10]
history=[]
coordlist=[(x[-1],y[-1],z[-1])]

i=0
direction=rd.randint(0,5)
history.append(direction)

while i<iterations-1:
        direction=rd.randint(0,5)
        if direction==0:
                x.append(x[-1]+1)
        elif direction==1:
                x.append(x[-1]-1)
        else:
                x.append(x[-1])
        if direction==2:
                y.append(y[-1]+1)
        elif direction==3:
                y.append(y[-1]-1)
        else:
                y.append(y[-1])
        if direction==4:
                z.append(z[-1]+1)
        elif direction==5:
                z.append(z[-1]-1)
        else:
                z.append(z[-1])
        comcoor=(x[-1],y[-1],z[-1])
        print(comcoor)
        if comcoor not in coordlist:
                print(('iterations',i,'rejections',rejections))
                coordlist.append(comcoor)
                i+=1
        else:
                print('oh poop',comcoor)
                x[-1]=x[-2]
                y[-1]=y[-2]
                z[-1]=z[-2]
                rejections+=1
                if rejections>iterations:
                        print('could not find a solution, breaking')
                        break
                        



fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(x,y,z)
ax.set_xlim3d([-10,10])
ax.set_xlabel('X')
ax.set_ylim3d([-10,10])
ax.set_ylabel('Y')
ax.set_zlim3d([-10,10])
ax.set_zlabel('Z')


plt.show()
