import pylab
import numpy, math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

##target_area = 1000.0
##
##r = (target_area / math.pi) ** 0.5
##m = numpy.zeros((2*r+2,2*r+2))
##
##a, b = r, r
##
##for row in range(0, m.shape[0]):
##    for col in range(0, m.shape[1]):
##        if (col-a)**2 + (row-b)**2 <= r**2:
##            m[row,col] = 1
def circle(radius):
    r = radius
    m = numpy.zeros((2*r+2,2*r+2))

    a, b = r, r

    x=[]
    y=[]
    for row in range(0, m.shape[0],60):
        for col in range(0, m.shape[1],60):
            if (col-a)**2 + (row-b)**2 >= (r-60)**2:
                if (col-a)**2 + (row-b)**2 <= r**2:
                    m[row,col] = 1
                    x.append(row-r)
                    y.append(col-r)
    return(x,y)

(x,y)=circle(360)

fig = plt.figure()
#plt.imshow(m)
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x,y,0,color="r",s=30)
pylab.axis('equal')
plt.show()

