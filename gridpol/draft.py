import math
import copy
import numpy as np
import gridpol8
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import collections
import funvec


## include in a text file later ?
kuhn=60
segradius=25
mtradius=20
mtlen=300
rkuhn=3*kuhn
nucradius=rkuhn
compaction=5000 #(bp/60nm)

##for glabrata
rdna={
11:[[1455689,375000]],
12:[[1402899,375000]]
}

chrlen={
0:491328,
1:502101,
2:558804,
3:651701,
4:687738,
5:927101,
6:992211,
7:1050361,
8:1100349,
9:1195129,
10:1302831,
11:1455689,
12:1402899
}

chrcen={
0:77251,
1:451123,
2:422097,
3:554532,
4:245929,
5:226190,
6:60727,
7:950000, ###pris a la louche sur du supplementary material, a verifier
8:780000, ###idem
9:217322,
10:257665,
11:987127,
12:1049554
}


####for pombe
##rdna={
###0:[[5000,20000],[5595000,200000]]
##2:[[5000,375000],[3490000,375000]],
###1:[[10000,750000],[150000,750000],[3095000,750000],[3495000,750000]]
###1:[[10000,5000],[150000,50000],[3095000,500000],[3495000,10000]]
##}
##
##chrlen={
###0:500000
##0:5600000,
##1:4500000,
##2:3500000,
###2:3500000+1500000, #homopoly conf.
##}
##
##chrcen={
###0:200000
##0:3780000,
##1:1623000,
##2:1136000,
##}

###for cerevisae
##rdna={
##12:[[450000,750000]]
##}
##
##chrlen={
##0:230218,
##1:813184,
##2:316620,
##3:1531933,
##4:576874,
##5:270161,
##6:1090940,
##7:562643,
##8:439888,
##9:745751,
##10:666816,
##11:1078177,
##12:924431,
##13:784333,
##14:1091291,
##15:948066
##}
##
##chrcen={
##0:151465,
##1:238207,
##2:114385,
##3:449711,
##4:151987,
##5:148510,
##6:496920,
##7:105586,
##8:355629,
##9:436307,
##10:440129,
##11:150828,
##12:268031,
##13:628758,
##14:326584,
##15:555957,
##}



# change chrcen and chrlen for rDNA insertion
insertion=0
for k in rdna.keys():
    print "k",k
    if k in rdna.keys() and k in chrlen.keys():
        for insert in range(len(rdna[k])):
            rdna[k][insert][0]=rdna[k][insert][0]+insertion
            chrlen[k]=chrlen[k]+rdna[k][insert][1]
            if rdna[k][insert][0]<=chrcen[k]:
                chrcen[k]=chrcen[k]+rdna[k][insert][1]
            print rdna[k][insert][0]<chrcen[k]
            insertion=insertion+rdna[k][insert][1]
            print "insertion",insertion

#mix the new configs to set polymer segments length
chrseg={}
for k in chrlen.keys():
    segnum=chrlen[k]/compaction
    chr=[]
#    segnum=chrlen[k]/compaction
#    chr=[]
    for seg in range(segnum):
        chr.append(kuhn)
    chrseg[k]=chr

#change relevant segments to rDNA
for k in chrseg:
    if k in rdna.keys():
        for insertions in rdna[k]:
            beg=insertions[0]/compaction
            end=(insertions[0]+insertions[1])/compaction
            print beg,end,k
            for seg in range(len(chrseg[k])):
#                print seg
                if seg in range(beg,end):
#                    print "replacing"
                    chrseg[k][seg]=rkuhn

#make an ordered python dictionary
#ochrlen=collections.OrderedDict(sorted(chrlen.items(), key=lambda t: t[1]))

radius=1000
occupied=[]
MTcenter=[0,0,840]

#pu_centromeres=gridpol8.circle(480,840,120) # known to converge after ~50 tries with a 1500nm nucleus
pu_centromeres=gridpol8.circle(480,MTcenter,120)
print len(pu_centromeres)

centromeres=set()
ranclist=[]
while len(centromeres)<len(chrlen):
    ranc=random.randint(0,len(pu_centromeres)-1)
    centromeres.update([pu_centromeres[ranc]])

centromeres=list(centromeres)
print len(centromeres)
occupied.extend(centromeres)

rarm=[]
larm=[]
rarm={}
larm={}
for i in range(len(chrlen)):
#    larm.append(chrcen[i])
#    rarm.append(chrlen[i]-chrcen[i])
    larm[i]=(chrcen[i])
    rarm[i]=(chrlen[i]-chrcen[i])

##divide chrseg in right and left arms
rchrseg={}
lchrseg={}
for k in chrseg.keys():
    lchrseg[k]=chrseg[k][0:chrcen[k]/compaction]
    lchrseg[k].reverse()
    rchrseg[k]=chrseg[k][chrcen[k]/compaction:chrlen[k]/compaction]

klist=[]
rsegcomp=[]
rkrocomp=[]
lsegcomp=[]
lkrocomp=[]
krstatus=[False]
klstatus=[False]
while False in krstatus or False in klstatus:
    krstatus=[]
    klstatus=[]
    occupied=[]
    for centro in range(len(centromeres)):
        print centro
        kr=gridpol8.gridpol5(centromeres[centro],(0,0,0),occupied,len(rchrseg[centro]),rchrseg[centro],kuhn,radius,-1000)
        occupied.extend(kr[0])
        rsegcomp.append(len(rchrseg[centro]))
        rkrocomp.append(len(kr[0]))
#        print len(occupied)
        kl=gridpol8.gridpol5(centromeres[centro],(0,0,0),occupied,len(lchrseg[centro]),lchrseg[centro],kuhn,radius,-1000)
        occupied.extend(kl[0])
        lsegcomp.append(len(lchrseg[centro]))
        lkrocomp.append(len(kl[0]))
#        print len(occupied)
        krstatus.append(kr[2])
        klstatus.append(kl[2])
        kl[0].reverse()
        kl[0].remove(kl[0][-1])
        k=kl[0]+kr[0]
        klist.append(k)

print "l vs s L",lsegcomp,lkrocomp
print "l vs s R",rsegcomp,rkrocomp


##tries=0
###klist=[]
##while False in krstatus or False in klstatus:
###    print "number of tries",tries
##    krstatus=[]
##    klstatus=[]
##    klist=[]
##    occupied=[]
##    occupied1=[]
##    occupied2=[]
##    occupied.extend(centromeres)
###    print len(occupied)
##    for centro in range(1):#in range(len(centromeres)):
##        kr=gridpol(centromeres[centro],(0,0,0),occupied,len(rchrseg[centro]),rchrseg[centro],kuhn,radius,-1000)
##        occupied1.extend(kr[0])
##        occupied.extend(kr[0])
##        krstatus.append(kr[2])
##        kl=gridpol(centromeres[centro],(0,0,0),occupied,len(lchrseg[centro]),lchrseg[centro],kuhn,radius,-1000)
##        kl[0].reverse()
##        kl[0].remove(kl[0][-1])
##        occupied2.extend(kl[0])
##        occupied.extend(kl[0])
##        klstatus.append(kl[2])
##        k=kl[0]+kr[0]
###        k=kr[0]
##        klist.append(k)
###        print centro
##        print centro,len(klist[centro])
##    tries+=1
##

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

centromeres=np.array(centromeres)
cx=centromeres[:,0]
cy=centromeres[:,1]
cz=centromeres[:,2]

pu_centromeres=np.array(pu_centromeres)
pcx=pu_centromeres[:,0]
pcy=pu_centromeres[:,1]
pcz=pu_centromeres[:,2]

ax.plot(cx,cy,cz,color='0')
ax.scatter(cx,cy,cz,color='b',s=50)
ax.scatter(pcx,pcy,pcz,color='0',s=10)

MTcm=[]
MTmat=[]
for i in range(len(centromeres)):
    vecMT=(centromeres[i]-MTcenter)/2
    MTcm.append(np.add(vecMT,MTcenter))
    ##get MT orientation
    nvecMT=funvec.norm3(vecMT)
    ##get rotationaxis
    zaxis=(0,0,1)
    rotaxis=funvec.cross(nvecMT,zaxis)
    ##get angle
    rotangle=math.acos(funvec.dot3(nvecMT,zaxis))
    rotmat=gridpol8.rotation_matrix(rotaxis,rotangle,'r')
    MTmat.append(rotmat)
    

dirveclist=[]
segposlist=[]
for k in range(len(klist)):
    klist[k]=np.array(klist[k])
    u1=klist[k][:,0]
    v1=klist[k][:,1]
    w1=klist[k][:,2]
    dirveclist.append(gridpol8.dirvec(u1,v1,w1))
    segposlist.append(gridpol8.midpoint(u1,v1,w1))
#    ax.plot(u1,v1,w1,color=str(float(k)/len(klist)))
#    ax.plot(u1,v1,w1,color=(random.random(),random.random(),random.random()))
    ax.plot(u1,v1,w1,color='k')
#    ax.scatter(u1,v1,w1,facecolors='none',color='r',s=20)
    if k==2:
        ax.scatter(u1[0:150],v1[0:150],w1[0:150],facecolors='none',color='r',s=20)
        ax.scatter(u1[-150:-2],v1[-150:-2],w1[-150:-2],facecolors='none',color='g',s=20)
    plt.draw()

u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]

xs=np.cos(u)*np.sin(v)
ys=np.sin(u)*np.sin(v)
zs=np.cos(v)

ax.plot_wireframe(xs*radius, ys*radius, zs*radius, color="r")
plt.draw()
plt.show()

initcoord=file("/Volumes/simunoyau/initcoords_nuc_3",'w')
initjoint=file("/Volumes/simunoyau/initjoints_nuc_3",'w')
initparm=file("/Volumes/simunoyau/initparm",'w')

print 'len(chrseg[0])',len(chrseg[0])
print 'len(segposlist[0])',len(segposlist[0])
print 'len(klist[0])',len(klist[0])

for i in chrcen.keys():
    initparm.write(str(chrcen[i]/5000)+"\n")

for k in range(len(chrseg)):
    print len(segposlist[k])
    for seg in range(len(segposlist[k])):
        x=segposlist[k][seg][0]*1e-9
        y=segposlist[k][seg][1]*1e-9
        z=segposlist[k][seg][2]*1e-9
        mtx=MTcm[k][0]*1e-9
        mty=MTcm[k][1]*1e-9
        mtz=MTcm[k][2]*1e-9
        if chrseg[k][seg]==kuhn:
            sizex=segradius
            sizey=segradius
            sizez=kuhn
        if chrseg[k][seg]==rkuhn:
            sizex=nucradius
            sizey=nucradius
            sizez=(nucradius*2)/25.
            print sizez
        if dirveclist[k][seg][0]==1:
            mat=gridpol8.rotation_matrix((1,0,0),math.pi/2,'r')
        if dirveclist[k][seg][0]==-1:
            mat=gridpol8.rotation_matrix((1,0,0),math.pi/-2,'r')
        if dirveclist[k][seg][1]==1:
            mat=gridpol8.rotation_matrix((0,1,0),math.pi/2,'r')
        if dirveclist[k][seg][1]==-1:
            mat=gridpol8.rotation_matrix((0,1,0),math.pi/-2,'r')
        if dirveclist[k][seg][2]==1:
            mat=gridpol8.rotation_matrix((0,0,1),math.pi/2,'r')
        if dirveclist[k][seg][2]==-1:
            mat=gridpol8.rotation_matrix((0,0,1),math.pi/-2,'r')
        initcoord.write("k"+str(k+1)+"."+str("%0.3d"%seg)+"\t"+"step 0.0"+"\t"+
        str("%.8e"%x)+"\t"+str("%.8e"%y)+"\t"+str("%.8e"%z)+"\t"+
        str("%.4f"%mat[0][0])+"\t"+str("%.4f"%mat[0][1])+"\t"+str("%.4f"%mat[0][2])+"\t"+
        str("%.4f"%mat[1][0])+"\t"+str("%.4f"%mat[1][1])+"\t"+str("%.4f"%mat[1][2])+"\t"+
        str("%.4f"%mat[2][0])+"\t"+str("%.4f"%mat[2][1])+"\t"+str("%.4f"%mat[2][2])+"\t"+
        str("%.1f"%0)+"\t"+str("%.1f"%0)+"\t"+str("%.1f"%0)+"\t"+
#        str(float(0))+"\t"+str(float(0))+"\t"+str(float(0))+"\t"+
        str("%0.3f"%(sizex/10))+"\t"+str("%0.3f"%(sizey/10))+"\t"+str("%0.3f"%(sizez/10))+
        "\n"
        )
    initcoord.write("MT"+str(k+1)+'.'+"\t"+"step 0.0"+"\t"+
    str("%.8e"%mtx)+"\t"+str("%.8e"%mty)+"\t"+str("%.8e"%mtz)+"\t"+
    str("%.4f"%MTmat[k][0][0])+"\t"+str("%.4f"%MTmat[k][0][1])+"\t"+str("%.4f"%MTmat[k][0][2])+"\t"+
    str("%.4f"%MTmat[k][1][0])+"\t"+str("%.4f"%MTmat[k][1][1])+"\t"+str("%.4f"%MTmat[k][1][2])+"\t"+
    str("%.4f"%MTmat[k][2][0])+"\t"+str("%.4f"%MTmat[k][2][1])+"\t"+str("%.4f"%MTmat[k][2][2])+"\t"+
    str("%.1f"%0)+"\t"+str("%.1f"%0)+"\t"+str("%.1f"%0)+"\t"+
#        str(float(0))+"\t"+str(float(0))+"\t"+str(float(0))+"\t"+
    str("%0.3f"%(mtradius/10))+"\t"+str("%0.3f"%(mtradius/10))+"\t"+str("%0.3f"%(mtlen/10))+
    "\n"
    )
    count=0
#    for joint in klist[k][1:-1]:
    for joint in klist[k]:
        x=joint[0]*1e-9
        y=joint[1]*1e-9
        z=joint[2]*1e-9
        initjoint.write("k"+str(k+1)+"."+str("%0.3d"%count)+"\t"+
                        str("%0.3e"%x)+"\t"+
                        str("%0.3e"%y)+"\t"+
                        str("%0.3e"%z)+"\t"+
                        "\n"
                        )
        count+=1
        
initcoord.close()
initjoint.close()
initparm.close()
