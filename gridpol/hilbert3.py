import funvec
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def hilbert3(n):
    xo = np.array(0)
    yo = np.array(0)
    zo = np.array(0)
    while n>=0:
        x = .5*np.array((.5+zo,.5+yo,-.5+yo,-.5-xo,-.5-xo,-.5-yo,.5-yo,.5+zo))
        y = .5*np.array((.5+xo,.5+zo,.5+zo,.5+yo,-.5+yo,-.5-zo,-.5-zo,-.5-xo))
        z = .5*np.array((.5+yo,-.5+xo,-.5+xo,.5-zo,.5-zo,-.5+xo,-.5+xo,.5-yo))
        xo=x
        yo=y
        zo=z
        n=n-1
    return(x.ravel(),y.ravel(),z.ravel())

def midpoint(x,y,z):
    xmid=[]
    ymid=[]
    zmid=[]
    for steps in range(len(x)-1):
        xmid.append((x[steps]+x[steps+1])/2.)
        ymid.append((y[steps]+y[steps+1])/2.)
        zmid.append((z[steps]+z[steps+1])/2.)
    return(np.array(xmid),np.array(ymid),np.array(zmid))

def dirvec(x,y,z):
    directionvec=[]
    for steps in range(len(x)-1):
        xdir=x[steps]-x[steps+1]
        ydir=y[steps]-y[steps+1]
        zdir=z[steps]-z[steps+1]
        directionvec.append(funvec.norm3((xdir,ydir,zdir)))
    return(np.array(directionvec))

order=4
(x,y,z)=hilbert3(order)

(a,b,c)=midpoint(x,y,z)

seglen=60 #nm

cubesec=[1]
evenstep=2
for i in range(order):
    print i
    cubesec.append(cubesec[i]+evenstep)
    evenstep=evenstep*2
    print cubesec,evenstep

resize=(cubesec[-1]*seglen)/2
mul=resize/x[0]

x=x*mul
y=y*mul
z=z*mul
a=a*mul
b=b*mul
c=c*mul

dirvec=dirvec(x,y,z)

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

ax.plot(x,y,z)
ax.scatter(x[0:2],y[0:2],z[0:2],color='0',s=50)

ax.plot(a,b,c)

plt.show()
