import copy
import numpy as np
import gridpol7
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import collections

chrlen={
0:230218,
1:813184,
2:316620,
3:1531933,
4:576874,
5:270161,
6:1090940,
7:562643,
8:439888,
9:745751,
10:666816,
11:1078177,
12:924431,
13:784333,
14:1091291,
15:948066
}

chrcen={
0:151465,
1:238207,
2:114385,
3:449711,
4:151987,
5:148510,
6:496920,
7:105586,
8:355629,
9:436307,
10:440129,
11:150828,
12:268031,
13:628758,
14:326584,
15:555957,
}

#make an ordered python dictionary
#ochrlen=collections.OrderedDict(sorted(chrlen.items(), key=lambda t: t[1]))


radius=1000
occupied=[]

pu_centromeres=gridpol7.circle(300,500,60)
print len(pu_centromeres)

centromeres=set()
ranclist=[]
while len(centromeres)<len(chrlen):
    ranc=random.randint(0,len(pu_centromeres)-1)
    centromeres.update([pu_centromeres[ranc]])

centromeres=list(centromeres)
print len(centromeres)
occupied.extend(centromeres)


klist=[]
krstatus=[False]
klstatus=[False]
rarm=[]
larm=[]
rarm={}
larm={}
for i in range(len(chrlen)):
#    larm.append(chrcen[i])
#    rarm.append(chrlen[i]-chrcen[i])
    larm[i]=chrcen[i]
    rarm[i]=chrlen[i]-chrcen[i]

olarm=collections.OrderedDict(sorted(larm.items(),key=lambda t: t[1]))
orarm=collections.OrderedDict(sorted(rarm.items(),key=lambda t: t[1]))

tries=0
while False in krstatus or False in klstatus:
    print "number of tries",tries
#    print "starting from scratch"
    krstatus=[]
    klstatus=[]
    klist=[]
    occupied=[]
    occupied.extend(centromeres)
    print len(occupied)
    for centro in range(len(centromeres)):
        print centro
        kr=gridpol7.gridpol2(centromeres[centro],(0,0,0),occupied,(int(round(rarm[centro]/5000.,0))),60,radius,-300)
#        kr=gridpol7.gridpol2(centromeres[centro],(0,0,0),occupied,40,60,radius,-300)
        occupied.extend(kr[0])
        krstatus.append(kr[1])
        kl=gridpol7.gridpol2(centromeres[centro],(0,0,0),occupied,(int(round(larm[centro]/5000.,0))),60,radius,-300)
#        kl=gridpol7.gridpol2(centromeres[centro],(0,0,0),occupied,40,60,radius,-300)
        kl[0].reverse()
        kl[0].remove(kl[0][-1])
        occupied.extend(kl[0])
        klstatus.append(kl[1])
        k=kl[0]+kr[0]
        klist.append(k)
#        print centro
    tries+=1

for i in range(len(chrlen)):
    for j in range(len(chrlen)):
        if i!=j:
            for el in klist[i]:
#            print el in klist[j]
                if el in klist[j]:
                    print "ALARM"
                    print (i,j)


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

centromeres=np.array(centromeres)
cx=centromeres[:,0]
cy=centromeres[:,1]
cz=centromeres[:,2]

pu_centromeres=np.array(pu_centromeres)
pcx=pu_centromeres[:,0]
pcy=pu_centromeres[:,1]
pcz=pu_centromeres[:,2]

#ax.plot(cx,cy,cz,color='0')
ax.scatter(cx,cy,cz,color='b',s=50)
ax.scatter(pcx,pcy,pcz,color='0',s=10)

for k in range(len(klist)):
    klist[k]=np.array(klist[k])
    u1=klist[k][:,0]
    v1=klist[k][:,1]
    w1=klist[k][:,2]
#    ax.plot(u1,v1,w1,color=str(float(k)/len(klist)))
    ax.plot(u1,v1,w1,color=(random.random(),random.random(),random.random()))
    plt.draw()

u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]

xs=np.cos(u)*np.sin(v)
ys=np.sin(u)*np.sin(v)
zs=np.cos(v)

ax.plot_wireframe(xs*radius, ys*radius, zs*radius, color="r")
plt.draw()
plt.show()

##x1=np.array(x1)
##
##u1=x1[:,0]
##v1=x1[:,1]
##w1=x1[:,2]
##
##(mu1,mv1,mw1)=midpoint(u1,v1,w1)
##
###u2=x2[:,0]
###v2=x2[:,1]
###w2=x2[:,2]
##
###ou1=occupied[:,0]
###ov1=occupied[:,1]
###ow1=occupied[:,2]
##
##fig = plt.figure()
##ax = fig.add_subplot(111, projection='3d')
##
##ax.plot(u1,v1,w1,color="g")
##ax.scatter(u1,v1,w1,color="g",s=10)
##ax.plot(mu1,mv1,mw1,color="b")
##ax.scatter(mu1,mv1,mw1,color="b",s=5)
###ax.plot(u2,v2,w2,color="m")
###ax.scatter(u2,v2,w2,color="m",s=10)
##
###ax.scatter(ou1,ov1,ow1,color="r",s=30)
##
##u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
##
##xs=np.cos(u)*np.sin(v)
##ys=np.sin(u)*np.sin(v)
##zs=np.cos(v)
##
##ax.plot_wireframe(xs*radius, ys*radius, zs*radius, color="r")
##
##plt.show()

##obj=file('/Volumes/Hua10TB/gridpol/obex.obj','w')
##
##obj.write("o noz \n")
##
##for i in range(len(u1)):
##    obj.write('v '+str(u1[i])+' '+str(v1[i])+' '+str(w1[i])+'\n')
##
##for i in range(len(u1)-1):
##    obj.write('f '+str(i+1)+' '+str(i+2)+'\n')
