from math import *
import numpy as np
import random as rd
import copy as cp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

iterations=500
kuhn=60
radius=5
rejections=0

def sub3(a, b):
        """Returns the difference between 3-vectors a and b."""
        return (a[0] - b[0], a[1] - b[1], a[2] - b[2])

def dist3(a, b):
        """Returns the distance between point 3-vectors a and b."""
        return len3(sub3(a, b))

def len3(v):
        """Returns the length of 3-vector v."""
        return sqrt(v[0]**2 + v[1]**2 + v[2]**2)

def randdir():
    direction=rd.randint(0,5)
    if direction == 0:
        return(1,0,0)
    elif direction == 1:
        return(-1,0,0)
    elif direction == 2:
        return(0,1,0)
    elif direction == 3:
        return(0,-1,0)
    elif direction == 4:
        return(0,0,1)
    elif direction == 5:
        return(0,0,-1)

x=[]
initcoord=np.array((0,0,0))
x.append(initcoord)
rejection_rate=0
while len(x)<iterations:
    stepback=initcoord
    initcoord=initcoord+randdir()
    d=dist3(initcoord,(0,0,0))
    if not any((initcoord==el).all() for el in x) and d<radius:
        x.append(initcoord)
        print (len(x),d,rejection_rate)
    else:
        initcoord=stepback
        rejection_rate+=1
        print "doing it again until it works"


x=np.array(x)

u=x[:,0]
v=x[:,1]
w=x[:,2]

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.plot(u,v,w)

plt.show()
