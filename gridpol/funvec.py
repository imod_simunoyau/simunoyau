import math

def len3(v):
        """Returns the length of 3-vector v."""
        return math.sqrt(v[0]**2 + v[1]**2 + v[2]**2)

def neg3(v):
        """Returns the negation of 3-vector v."""
        return (-v[0], -v[1], -v[2])

def add3(a, b):
        """Returns the sum of 3-vectors a and b."""
        return (a[0] + b[0], a[1] + b[1], a[2] + b[2])

def sub3(a, b):
        """Returns the difference between 3-vectors a and b."""
        return (a[0] - b[0], a[1] - b[1], a[2] - b[2])

def mul3(v, s):
        """Returns 3-vector v multiplied by scalar s."""
        return (v[0] * s, v[1] * s, v[2] * s)

def div3(v, s):
        """Returns 3-vector v divided by scalar s."""
        return (v[0] / s, v[1] / s, v[2] / s)

def dist3(a, b):
        """Returns the distance between point 3-vectors a and b."""
        return len3(sub3(a, b))

def norm3(v):
        """Returns the unit length 3-vector parallel to 3-vector v."""
        l = len3(v)
        if (l > 0.0): return (v[0] / l, v[1] / l, v[2] / l)
        else: return (0.0, 0.0, 0.0)

def dot3(a, b):
        """Returns the dot product of 3-vectors a and b."""
        return (a[0] * b[0] + a[1] * b[1] + a[2] * b[2])

def cross(a, b):
        """Returns the cross product of 3-vectors a and b."""
        return (a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2],
                a[0] * b[1] - a[1] * b[0])

def project3(v, d):
        """Returns projection of 3-vector v onto unit 3-vector d."""
        return mul3(v, dot3(norm3(v), d))

