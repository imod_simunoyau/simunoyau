import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def hilbert3(n):
	xo = np.array(0)
	yo = np.array(0)
	zo = np.array(0)
	while n>=0:
		x = .5*np.array((.5+zo,.5+yo,-.5+yo,-.5-xo,-.5-xo,-.5-yo,.5-yo,.5+zo))
		y = .5*np.array((.5+xo,.5+zo,.5+zo,.5+yo,-.5+yo,-.5-zo,-.5-zo,-.5-xo))
		z = .5*np.array((.5+yo,-.5+xo,-.5+xo,.5-zo,.5-zo,-.5+xo,-.5+xo,.5-yo))
		xo=x
		yo=y
		zo=z
		n=n-1
	return(x.ravel(),y.ravel(),z.ravel())

def midpoint(x,y,z):
	xmid=[]
	ymid=[]
	zmid=[]
	for steps in range(len(x)-1):
		xmid.append((x[steps]+x[steps+1])/2.)
                ymid.append((y[steps]+y[steps+1])/2.)
                zmid.append((z[steps]+z[steps+1])/2.)
	return(xmid,ymid,zmid)

(x,y,z)=hilbert3(4)

(a,b,c)=midpoint(x,y,z)

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

ax.plot(x,y,z)

ax.plot(a,b,c)

plt.show()
