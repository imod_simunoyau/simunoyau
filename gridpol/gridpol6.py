from math import *
import numpy as np
import random as rd
import copy as cp


iterations=4000
kuhn=int(60)
radius=1000
rejections=0

def circle(radius,height):
    z = height
    r = radius
    m = np.zeros((2*r+2,2*r+2))

    a, b = r, r
    coord=[]
    x=[]
    y=[]
    for row in range(0, m.shape[0],60):
        for col in range(0, m.shape[1],60):
            if (col-a)**2 + (row-b)**2 >= (r-60)**2:
                if (col-a)**2 + (row-b)**2 <= r**2:
                    m[row,col] = 1
                    coord.append((row-r,col-r,z))
#                    x.append(row-r)
#                    y.append(col-r)
                    
    return(coord)

def sub3(a, b):
        """Returns the difference between 3-vectors a and b."""
        return (a[0] - b[0], a[1] - b[1], a[2] - b[2])

def dist3(a, b):
        """Returns the distance between point 3-vectors a and b."""
        return len3(sub3(a, b))

def len3(v):
        """Returns the length of 3-vector v."""
        return sqrt(v[0]**2 + v[1]**2 + v[2]**2)

def randdir(kuhn):
    direction=rd.randint(0,5)
    if direction == 0:
        return(kuhn,0,0)
    elif direction == 1:
        return(-kuhn,0,0)
    elif direction == 2:
        return(0,kuhn,0)
    elif direction == 3:
        return(0,-kuhn,0)
    elif direction == 4:
        return(0,0,kuhn)
    elif direction == 5:
        return(0,0,-kuhn)

def randdir2D(kuhn):
    direction=rd.randint(0,3)
    if direction == 0:
        return(kuhn,0,0)
    elif direction == 1:
        return(-kuhn,0,0)
    elif direction == 2:
        return(0,kuhn,0)
    elif direction == 3:
        return(0,-kuhn,0)

def gridpol(start,iterations,kuhn,radius,exclusion_zone):
    coords=[]
#    initcoord=np.array(initcoord)#np.array((0,0,0))
    initcoord=start
    coords.append(initcoord)
    rejection_rate=0
    while len(coords)<iterations:
        stepback=initcoord
#        print stepback
        x=initcoord[0]
        y=initcoord[1]
        z=initcoord[2]
        r=randdir(kuhn)
        rx=r[0]
        ry=r[1]
        rz=r[2]
        initcoord=(x+rx,y+ry,z+rz)
        d=dist3(initcoord,(0,0,0))
#        if not any((initcoord==el).all() for el in x) and d<radius and initcoord[2]>exclusion_zone:
#        print initcoord not in coords
        if initcoord not in coords and d<radius and initcoord[2]>exclusion_zone:
            coords.append(initcoord)
#            print (coords)
#            print (initcoord)
            print (len(coords),d,rejection_rate)
        else:
            initcoord=stepback
            rejection_rate+=1
            print "doing it again until it works"
        if rejection_rate>30:
            print "screw this, starting all over"
            index=len(coords)
            if len(coords)>30:
                del coords[index-20:index]
                initcoord=coords[-1]
            if len(coords)<=20:
                coords=[]
                initcoord=start
                coords.append(initcoord)
            rejection_rate=0
    return(coords)

def gridpol2(start,center,occupied,iterations,kuhn,radius,exclusion_zone):
    coords=[]
    initcoord=start
    coords.append(initcoord)
    rejection_rate=0
    reset=0
#    if exclusion_zone=[]
    while len(coords)<iterations:
        stepback=initcoord
        x=initcoord[0]
        y=initcoord[1]
        z=initcoord[2]
        r=randdir(kuhn)
        rx=r[0]
        ry=r[1]
        rz=r[2]
        initcoord=(x+rx,y+ry,z+rz)
        d=dist3(initcoord,center)
        if initcoord not in coords and initcoord not in occupied and d<radius and initcoord[2]>exclusion_zone:
            coords.append(initcoord)
#            print (len(coords),reset,rejection_rate)
        else:
            initcoord=stepback
            rejection_rate+=1
        if rejection_rate>30:
            index=len(coords)
            if len(coords)>30:
                del coords[index-20:index]
                initcoord=coords[-1]
            if len(coords)<=20:
                coords=[]
                initcoord=start
                coords.append(initcoord)
            if reset>=5000:
                print "could not converge"
                break
            rejection_rate=0
            reset+=1
    return(coords)

def midpoint(x,y,z):
    xmid=[]
    ymid=[]
    zmid=[]
    for steps in range(len(x)-1):
        xmid.append((x[steps]+x[steps+1])/2.)
        ymid.append((y[steps]+y[steps+1])/2.)
        zmid.append((z[steps]+z[steps+1])/2.)
    return(np.array(xmid),np.array(ymid),np.array(zmid))

def dirvec(x,y,z):
    directionvec=[]
    for steps in range(len(x)-1):
        xdir=x[steps]-x[steps+1]
        ydir=y[steps]-y[steps+1]
        zdir=z[steps]-z[steps+1]
        directionvec.append(funvec.norm3((xdir,ydir,zdir)))
    return(np.array(directionvec))  
