#    <This is a script that model nuclear interactions as systems of rigid bodies>
#    Copyright (C) 2008  Hua WONG <wong.hua____at__gmail__dot_com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
from sys import *

lenKuhn=argv[1]
diamTL=argv[2]
compaction=argv[3]
diamNC=argv[4]
diamRDNA=argv[5]
topo=argv[6]

mjob=file('/home1/Imod/hwong/jobs/KT/job'+argv[1]+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_"+argv[5]+"_"+argv[6],"w")

mjob.write("""
#!/bin/bash
#
#$ -cwd
#$ -j y
#$ -S /bin/bash
#$ -M hwong@pasteur.fr
#$ -m ea
#$ -q imod
#$ -o /home1/Imod/hwong/jobs/outputs

export PATH="/home1/Imod/hwong/apps/bin:/home1/Imod/hwong/apps/blender:${PATH}"
export DYLD_LIBRARY_PATH="/home1/Imod/hwong/apps/lib:${DYLD_LIBRARY_PATH}"
export LD_LIBRARY_PATH="/home1/Imod/hwong/apps/lib:${LD_LIBRARY_PATH}"

echo $PATH
echo $DYLD_LIBRARY_PATH
echo $LD_LIBRARY_PATH
""")

mjob.write("#$ -N sim"+argv[1]+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_"+argv[5]+"_"+argv[6]+"\n"+"\n")

mjob.write("python /home1/Imod/hwong/scripts/Simulation_V4.4_pull_koszul_translocation.py "+
	"/home1/Imod/hwong/scripts/massive/masscoordparmKT/InitCoord"+argv[1]+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_"+argv[5]+"_"+argv[6]+
	" "+
	"/pasteur/simunoyau/trajectoriesXMLKT/traj"+argv[1]+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_"+argv[5]+"_"+argv[6]+"/simu"+argv[1]+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_"+argv[5]+"_"+argv[6]+
	" "+
	"1"+
	" "+
	str(round((float(diamNC)*1e9)/1000.))+
	" "+
        "/home1/Imod/hwong/scripts/massive/masscoordparmKT/parm"+argv[1]+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_"+argv[5]+"_"+argv[6]+
	" "+
	argv[6]
	)

#python 
#/home1/Imod/hwong/scripts/Simulation_V3.2.py 
#/home1/Imod/hwong/scripts/InitcoordK5Longest_EctopicNuc 
#/pasteur/simunoyau/trajectoriesXML/FatterRNA/Shuffle1/V3simu_CEN5300seg30nmLP200nmRDNA_100nmTEL_radius1.0/simu 0 1


mjob.close
