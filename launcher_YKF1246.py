#    <This is a script that model nuclear interactions as systems of rigid bodies>
#    Copyright (C) 2008  Hua WONG <wong.hua____at__gmail__dot_com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
from os import *
import shutil

#shutil.rmtree('/home1/Imod/hwong/jobs/massive16')
mkdir('/home1/Imod/hwong/jobs/massive21KTYKF1246')
chdir('/home1/Imod/hwong/jobs/massive21KTYKF1246')
#mkdir('/home1/Imod/hwong/jobs/massive21')

#argv for fastcoordmaker
lenKuhn=[60e-9]
diamTL=[25e-9]#,400e-9)
compaction=[11]
diamNC=[2000e-9]
diamRDNA=[200e-9]
topostep=[\
'9000000.1',\
'9000000.2',\
'9000000.3',\
'9000000.4',\
'9000000.5',\
'9000000.6',\
'9000000.7',\
'9000000.8',\
'9000000.9',\
'9000000.10',\
'9000000.11',\
'9000000.12',\
'9000000.13',\
'9000000.14',\
'9000000.15',\
'9000000.16',\
'9000000.17',\
'9000000.18',\
'9000000.19',\
'9000000.20'\
]

simlist=[]

taille=0
for kuhn in lenKuhn:
	for tel in diamTL:
		for compact in compaction:
			for NC in diamNC:
				for RDNA in diamRDNA:
					for topo in topostep:
						print kuhn,tel,compact,NC,RDNA,topo,taille
						system('python /home1/Imod/hwong/scripts/massive/fastcoordmaker_v2.8_koszul_transloc_YKF1246.py'+" "+\
							str(kuhn)+" "+\
							str(tel)+" "+\
							str(compact)+" "+\
							str(NC)+" "+\
							str(RDNA)+" "+\
							str(topo))
						system('python /home1/Imod/hwong/scripts/massive/jobmaker_YKF1246.py'+" "+\
							str(kuhn)+" "+\
							str(tel)+" "+\
							str(compact)+" "+\
							str(NC)+" "+\
							str(RDNA)+" "+\
							str(topo))
						simlist.append(str(kuhn)+"_"+\
							str(tel)+"_"+\
							str(compact)+"_"+\
							str(NC)+"_"+\
							str(RDNA)+"_"+\
							str(topo))
						taille+=1

long=len(simlist)

for i in simlist[0:long]:
	system('qsub /home1/Imod/hwong/jobs/massive21KTYKF1246/massjob'+i)
