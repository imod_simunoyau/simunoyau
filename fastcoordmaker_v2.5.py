#    <This is a script that model nuclear interactions as systems of rigid bodies>
#    Copyright (C) 2008  Hua WONG <wong.hua____at__gmail__dot_com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
from scipy import *

from random import *

from math import pi

from sys import *

def rotx(t):
	ct=cos(t)
	st=sin(t)
	m00 = 1
	m01 = 0
	m02 = 0
	m10 = 0
	m11 = ct
	m12 = st
	m20 = 0
	m21 = -st
	m22 = ct
	return (array([[m00,m01,m02],[m10,m11,m12],[m20,m21,m22]]))


def roty(t):
	ct=cos(t)
	st=sin(t)
	m00 = ct
	m01 = 0
	m02 = -st
	m10 = 0
	m11 = 1
	m12 = 0
	m20 = st
	m21 = 0
	m22 = ct
	return (array([[m00,m01,m02],[m10,m11,m12],[m20,m21,m22]]))


def rotz(t):
	ct=cos(t)
	st=sin(t)
	m00 = ct
	m01 = st
	m02 = 0
	m10 = -st
	m11 = ct
	m12 = 0
	m20 = 0
	m21 = 0
	m22 = 1
	return (array([[m00,m01,m02],[m10,m11,m12],[m20,m21,m22]]))


def euler2mat(x,y,z):
	matx=rotx(x)
	maty=roty(y)
	matz=rotz(z)
	return dot((dot(matx,maty)),matz)
	
#while len(argv)<2:
#	print "rappel, lenKuhn,diamTL,compaction en nm/kb, diamNC"
#	break 
#else:
#	pass

#outputfiles
#InitCoord=file('InitCoord',"w")
InitCoord=file('InitCoord'+argv[1]+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_"+argv[5]+"_"+argv[6],"w")
#parmfiles=file('parm',"w")
parmfiles=file('parm'+argv[1]+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_"+argv[5]+"_"+argv[6],"w")

#sim parameters
numK=16

diamCH=20e-9
lenKuhn=60e-9
lenKuhn=float(argv[1])

diamTL=100e-9
diamTL=float(argv[2])
lenTL=diamTL/100.

diamMT=25e-9
#lenMT=300e-9 #nm
lenMT={
0:950e-9,
1:950e-9,
2:950e-9,
3:950e-9,
4:950e-9,
5:950e-9,
6:950e-9,
7:950e-9,
8:950e-9,
9:950e-9,
10:950e-9,
11:950e-9,
12:950e-9,
13:950e-9,
14:950e-9,
15:950e-9
} #nm

#precision=5000#bp
compaction=13#nm/kb
compaction=int(argv[3])
precision=int(round(lenKuhn*1e9/compaction)*1000)

diamRDNA=200e-9
diamRDNA=float(argv[5])
lenRDNA=diamRDNA/25.#100.
precisionRDNA=5000

#incpos=(5,150000) #which chromosome, which position in bp
#inclen=1525000

incpos=(12,450000) #which chromosome, which position in bp
inclen=750000 #in bp


diamNC=2000e-9
diamNC=float(argv[4])
spbloc=diamNC/2.-100e-9
spbdiam=110e-9
spbexc=110e-9+30e-9

anginc=(2*pi)/16

#chromosome length from SGD
chrlen={
0:230218,
1:813184,
2:316620,
3:1531933,
4:576874,
5:270161,
6:1090940,
7:562643,
8:439888,
9:745751,
10:666816,
11:1078177,
12:924431,
13:784333,
14:1091291,
15:948066
}

chrcen={
0:151465,
1:238207,
2:114385,
3:449711,
4:151987,
5:148510,
6:496920,
7:105586,
8:355629,
9:436307,
10:440129,
11:150828,
12:268031,
13:628758,
14:326584,
15:555957,
}

for i in range(numK):
	parmfiles.write(str((chrcen[i]/precision)-1)+"\n")

parmfiles.close()

MT={}
K={}

for i in range(numK):
#create MTs
	MT[i]=zeros(3)

#add N rdna segments if matches incpos[0]
	if i==incpos[0]-1:
		#K[i]=zeros(((chrlen[i]+inclen)/precision,3)) # commented to have consistant RDNA length
		rdnaseg=inclen/precisionRDNA
		print rdnaseg
		K[i]=zeros(((chrlen[i]/precision)+rdnaseg,3))
#		print range(0,incpos[1]/precision),range(incpos[1]/precision,(incpos[1]/precision)+inclen/precision),range((incpos[1]/precision)+inclen/precision,chrlen[i]/precision)
#		for j in range(0,(chrlen[i]+inclen)/precision):
		for j in range(0,(chrlen[i]/precision)+rdnaseg):
			K[i][j][2]=j*-lenKuhn
		print ((chrlen[i]/precision)+rdnaseg),chrlen[i],precision
	else:
#create normal linear chromosomes
		K[i]=zeros((chrlen[i]/precision,3))
		for j in range(K[i].shape[0]):
			K[i][j][2]=j*-lenKuhn
#special case of the telomeres
	K[i][0][2]=K[i][0][2]+((diamTL/2.)-(lenKuhn/2.))
	K[i][-1][2]=K[i][-1][2]-((diamTL/2.)-(lenKuhn/2.))
#special case of the RDNA
#print incpos[1]/precision
#print "-1",K[incpos[0]-1][(incpos[1]/precision)-1][2]
#print " 0",K[incpos[0]-1][(incpos[1]/precision)][2]
#print "+1",K[incpos[0]-1][(incpos[1]/precision)+1][2]


center=K[incpos[0]-1][(incpos[1]/precision),2]
K[incpos[0]-1][(incpos[1]/precision)::1,2]=K[incpos[0]-1][(incpos[1]/precision)::1,2]-K[incpos[0]-1][(incpos[1]/precision),2]
K[incpos[0]-1][(incpos[1]/precision)::1,2]=K[incpos[0]-1][(incpos[1]/precision)::1,2]*diamRDNA/lenKuhn
K[incpos[0]-1][(incpos[1]/precision)::1,2]=K[incpos[0]-1][(incpos[1]/precision)::1,2]+center

center=K[incpos[0]-1][((incpos[1]+inclen)/precision),2]
K[incpos[0]-1][((incpos[1]+inclen)/precision)::1,2]=K[incpos[0]-1][((incpos[1]+inclen)/precision)::1,2]-K[incpos[0]-1][((incpos[1]+inclen)/precision),2]
K[incpos[0]-1][((incpos[1]+inclen)/precision)::1,2]=K[incpos[0]-1][((incpos[1]+inclen)/precision)::1,2]*lenKuhn/diamRDNA
K[incpos[0]-1][((incpos[1]+inclen)/precision)::1,2]=K[incpos[0]-1][((incpos[1]+inclen)/precision)::1,2]+center

#print "-1",K[incpos[0]-1][(incpos[1]/precision)-1][2]
#print "+0",K[incpos[0]-1][(incpos[1]/precision)][2]
#print "+1",K[incpos[0]-1][(incpos[1]/precision)+1][2]

#randomize chromosomes comment shuffle line to disable
klist=range(numK)
randklist=shuffle(klist)

#generate the bulk of the chromosome
increment=0
for k in klist:
	#CH rotation matrix
	chmat=euler2mat(0,0,(increment*(anginc)))
	aa=chmat[0][0]
	ab=chmat[0][1]
	ac=chmat[0][2]
	ba=chmat[1][0]
	bb=chmat[1][1]
	bc=chmat[1][2]
	ca=chmat[2][0]
	cb=chmat[2][1]
	cc=chmat[2][2]
	for seg in range(len(K[k])):
		K[k][seg][1]=lenMT[k]+(spbexc+diamCH)/2.
		K[k][seg][2]=K[k][seg][2]+(((chrcen[k]/precision)-1)*lenKuhn+spbloc)
		K[k][seg]=dot(chmat,K[k][seg])
		x=K[k][seg][0]
		y=K[k][seg][1]
		z=K[k][seg][2]
		if seg==0 or seg==len(K[k])-1:
			sizex=diamTL
			sizey=sizex
			sizez=lenTL
			#print sizex,sizey,sizez
		#elif k==incpos[0]-1 and incpos[1]/precision<=seg<=(incpos[1]+inclen)/precision:
		elif k==incpos[0]-1 and incpos[1]/precision<=seg<=(incpos[1]/precision)+rdnaseg:
			sizex=diamRDNA
			sizey=sizex
			sizez=lenRDNA
			#print sizex,sizey,sizez,'toto'
		else:
			sizex=diamCH
			sizey=sizex
			sizez=lenKuhn
			#print sizex,sizey,sizez
		InitCoord.write("k"+str(k+1)+"."+str("%0.3d"%seg)+"\t"+"step 0.0"+"\t"+
		str("%.8e"%x)+"\t"+str("%.8e"%y)+"\t"+str("%.8e"%z)+"\t"+
		str("%.4f"%aa)+"\t"+str("%.4f"%ab)+"\t"+str("%.4f"%ac)+"\t"+
		str("%.4f"%ba)+"\t"+str("%.4f"%bb)+"\t"+str("%.4f"%bc)+"\t"+
		str("%.4f"%ca)+"\t"+str("%.4f"%cb)+"\t"+str("%.4f"%cc)+"\t"+
		str(float(0))+"\t"+str(float(0))+"\t"+str(float(0))+"\t"+
		str("%0.3f"%(sizex*1e8))+"\t"+str("%0.3f"%(sizey*1e8))+"\t"+str("%0.3f"%(sizez*1e8))+
		"\n"
		)
	seg=len(K[k])+1
	if seg==len(K[k])+1:
		#MT rotation matrix
		mat1=euler2mat(0,0,(increment*(anginc)))
		mat2=euler2mat((-pi/2),0,0)
		mtmat=dot(mat1,transpose(mat2))
		#mtmat=euler2mat(-(pi/2),0,(increment*(anginc)))
		tmtmat=transpose(mtmat)
		aa=mtmat[0][0]
		ab=mtmat[0][1]
		ac=mtmat[0][2]
		ba=mtmat[1][0]
		bb=mtmat[1][1]
		bc=mtmat[1][2]
		ca=mtmat[2][0]
		cb=mtmat[2][1]
		cc=mtmat[2][2]
	
		MT[k][1]=lenMT[k]/2.+spbexc/2.
		MT[k][2]=MT[k][2]+spbloc
		MT[k]=dot(mat1,MT[k])
		mtx=MT[k][0]
		mty=MT[k][1]
		mtz=MT[k][2]
	
		InitCoord.write(
		"MT"+str(k+1)+"."+"\t"+"step 0.0"+"\t"+
		str("%.8e"%mtx)+"\t"+str("%.8e"%mty)+"\t"+str("%.8e"%mtz)+"\t"+
		str("%.4f"%aa)+"\t"+str("%.4f"%ab)+"\t"+str("%.4f"%ac)+"\t"+
		str("%.4f"%ba)+"\t"+str("%.4f"%bb)+"\t"+str("%.4f"%bc)+"\t"+
		str("%.4f"%ca)+"\t"+str("%.4f"%cb)+"\t"+str("%.4f"%cc)+"\t"+
		str(float(0))+"\t"+str(float(0))+"\t"+str(float(0))+"\t"+
		str("%0.3f"%(diamMT*1e8))+"\t"+str("%0.3f"%(diamMT*1e8))+"\t"+str("%0.3f"%(lenMT[k]*1e8))+
		"\n"
		)
	increment+=1


#InitCoord.write(bases[b].name+"\t"+"step "+str(total_time/dt)+"\t"+
#   str("%.8e"%x)+"\t"+str("%.8e"%y)+"\t"+str("%.8e"%z)+
#   "\t"+str("%.4f"%aa)+"\t"+str("%.4f"%ab)+"\t"+str("%.4f"%ac)+"\t"+
#   str("%.4f"%ba)+"\t"+str("%.4f"%bb)+"\t"+str("%.4f"%bc)+"\t"+str("%.4f"%ca)+"\t"+
#   str("%.4f"%cb)+"\t"+str("%.4f"%cc)+"\t"+str(vel[b][0])+"\t"+str(vel[b][1])+"\t"+
#   str(vel[b][2])+"\t"+str("%0.3f"%(Ksizes[b][0]))+"\t"+str("%0.3f"%(Ksizes[b][1]))+"\t"+str("%0.3f"%(Ksizes[b][2]))+"\n")
#

InitCoord.close()
