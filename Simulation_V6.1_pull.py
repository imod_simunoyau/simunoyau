#    <This is a script that model nuclear interactions as systems of rigid bodies>
#    Copyright (C) 2008  Hua WONG <wong.hua____at__gmail__dot_com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    <This script uses rigid body systems to do nuclear models>
#    Copyright (C) 2008-2013  Hua WONG
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


#working version

#blender -b ODEviz.blend -P ODEviz.py &

from numpy import *
from math import *
import sys
from sys import *
import socket
import os
import time
import xml.dom.minidom
from xml.dom.minidom import Document
import scipy
import scipy.io
from scipy.io import netcdf
from scipy import *
from string import *
import gzip
from random import *
import ode
import string

"""
for i in Ob:
    Scn.unlink(i)
Blender.Redraw()
"""

#administratif
initdirectory=0
directorysave=10000

#valeurs generale
bodies=[]
geoms=[]
init=0
print(argv)
n_chromosomes = int(argv[8])
microtubule=True
Nucleus=True
RepNoyau='/home1/Imod/hwong/default_nucleus_conf/icosph/'


#valeurs physique
Force=0.1*pow(10,-12) # pN
N=50
spacing=3.4*1e-9 #m
longueur=30
lx,ly,lz=2*1e-9,2*1e-9,3.4*1e-9
euX,euY,euZ=0,0,0
k=1.38*pow(10,-23)
#T=0#300
topostep=int(float(argv[6]))
sampling=100
sphradius=15e-9 #m
M=ode.Mass()
#Ma=7000*(1.7*pow(10,-27)) #kg
Ma=(13e6*1.7*pow(10,-27))/2.
M.setSphereTotal(Ma,sphradius)
M.mass = Ma
#M.setBox(0.1,lx,ly,lz) #m
#M.mass=Ma
SM=ode.Mass()
SM.setSphere(7900,10*1e-9)
mul=10
SM.mass=mul*Ma
viscosity=6.6e-3
#dt=(N*log(10)*Ma)/(6*pi*(0.89E-3)*lx/2) #s
#dt = ((N*log(10)*Ma)/(6*pi*(viscosity)*sphradius))
host=socket.gethostname()
pidnum=os.getpid()
damp=(1-pow(10,-N))/(N*log(10))
#diff=(k*T)/(6*pi*6.6E-3*sphradius)
drag=-6*pi*sphradius*viscosity
kB=1.38*pow(10.,-23.)
temp=50.
friction=6.*pi*viscosity*20.*pow(10.,-9.)
TempsFriction=Ma/friction
dt=TempsFriction
duration=3000000*dt
cooldown=1000000*dt
sigma=1000.*sqrt(2.*friction*kB*temp/dt)
f10_8=pow(10.,8.)
f10_minus8=pow(10.,-8.)
intfreq=[]
#colmatrix=zeros([2726,2726])
stickiness=0.0
#print "diff",diff
#print "k",k
#print "T",T
print "sphradius",sphradius
print "dt",dt



#Arg1: corps, arg2: sigma, arg3: friction
def langevin_tr(arg1,arg2,arg3):
    argvb=arg1.getLinearVel()
    """argfx=gauss(0.,arg2)-arg3*argvb[0]
    argfy=gauss(0.,arg2)-arg3*argvb[1]
    argfz=gauss(0.,arg2)-arg3*argvb[2]"""
    argfx=2.*arg2*(.5-random())-arg3*argvb[0]
    argfy=2.*arg2*(.5-random())-arg3*argvb[1]
    argfz=2.*arg2*(.5-random())-arg3*argvb[2]
    arg1.addForce((argfx,argfy,argfz))


def stickycol(stickygeom,colgeom,contactgroup,stickiness,id):
    contactstick=ode.collide(stickygeom,colgeom)
    stickypos=stickygeom.getBody().getPosition()
    randnum=uniform(0,1)

    if contactstick!=[] and total_time/dt>1000000:
        if randnum<stickiness:
            if len(contactgroup)<10:
                contactgroup.append(ode.JointGroup())
                stick=ode.BallJoint(world,contactgroup[-1])
                stick.attach(stickygeom.getBody(),None)
                stick.setAnchor(stickygeom.getPosition())
        if randnum>stickiness:
            if contactgroup!=[]:
                randvar=randint(0,len(contactgroup)-1)
                contactgroup[randvar].empty()
                contactgroup.remove(contactgroup[randvar])

    if contactstick==[] and total_time/dt>1000000:
        if randnum<stickiness:
            pass
        if randnum>stickiness:
            pass

def obj2tri(wavefront_obj,n): #import wavefront obj to be used for trimesh generation
    cellwallobj=file(wavefront_obj,'r')
    cellwalldat=cellwallobj.readlines()
    vertlist=[]
    facelist=[]
    nuc1=[]
    for lines in cellwalldat:
        if lines[0]=='v':
            linecontent=string.split(lines)
            vertlist.append(linecontent)
        elif lines[0]=='f':
            linecontent=string.split(lines)
            #print linecontent
            line = ['f'] + [ str(int(num) - 1) for num in linecontent[1:]]
            facelist.append(line)
    for el in vertlist:
        el.remove(el[0])
        el[0]=float(el[0])*1e-6
        el[1]=float(el[1])*1e-6
        el[2]=float(el[2])*1e-6
    for el in range(len(vertlist)):
        vertlist[el]=tuple(vertlist[el])
    for el in facelist:
        el.remove(el[0])
        el[0]=int(el[0])
        el[1]=int(el[1])
        el[2]=int(el[2])
        if n==-1:
            el.reverse()
    for el in range(len(facelist)):
        facelist[el]=tuple(facelist[el])
    return(vertlist,facelist)

def TrimeshCollision(blendobj,normals,world,space):
    mesh=NMesh.GetRawFromObject(blendobj)
    #scale=Object.Get(blendobj).getSize()[0]
    VERTNUC=[]
    INDNUC=[]
    for i in range(len(mesh.verts)):
        VERTNUC.append((mesh.verts[i].co[0]*1e-8,mesh.verts[i].co[1]*1e-8,mesh.verts[i].co[2]*1e-8))
    for j in range(len(mesh.faces)):
        ind=()
        idx=[]
        for k in range(len(mesh.faces[j].v)):
            idx.append(mesh.faces[j].v[k].index)
        if normals==0:
            idx.reverse() #<---- uncomment to reverse normals
        else :
            pass
        ind=ind+(idx[0],idx[1],idx[2],)
        idx=[]
        INDNUC.append(ind)
    objtrimesh = ode.TriMeshData()
    objtrimesh.build(VERTNUC, INDNUC)
    geom = ode.GeomTriMesh(objtrimesh, space)
    return geom

def scalp (vec, scal):
    vec[0] *= scal
    vec[1] *= scal
    vec[2] *= scal

def localPull(body,destination):
    l=list(\
        (body.getPosition()[0]-destination[0],\
        body.getPosition()[1]-destination[1],\
        body.getPosition()[2]-destination[2])\
        )
    scalp(l, (-500 / len3 (l))*pow(10,-12))
    body.addForce(l)
    
def pull():
    global bodies
    for b in bodies:
        l=list (b.getPosition ())
        scalp (l, (-1000 / len3 (l))*pow(10,-11))
        b.addForce(l)

def attract(xyzpos,body2,dist):
        x1=xyzpos[0]
        x2=body2.getPosition()[0]
        y1=xyzpos[1]
        y2=body2.getPosition()[1]
        z1=xyzpos[2]
        z2=body2.getPosition()[2]
        l=list(\
                (\
                x1-x2,\
                y1-y2,\
                z1-z2\
                )\
              )
        scalp(l,40000*pow(10,-4))
        if dist3((x1,y1,z1),(x2,y2,z2))>=dist:
                body2.addForce(l)
                body1.addForce(neg3(l))
        if dist3((x1,y1,z1),(x2,y2,z2))<=dist:
                body2.addForce(neg3(l))
                body1.addForce(l)

def attract2(body1,body2,dist):
        x1=body1.getPosition()[0]
        x2=body2.getPosition()[0]
        y1=body1.getPosition()[1]
        y2=body2.getPosition()[1]
        z1=body1.getPosition()[2]
        z2=body2.getPosition()[2]
        l=list(\
                (\
                x1-x2,\
                y1-y2,\
                z1-z2\
                )\
              )
        scalp(l,40000*pow(10,-4))
        if dist3((x1,y1,z1),(x2,y2,z2))>=dist:
                body2.addForce(l)
                body1.addForce(neg3(l))
        if dist3((x1,y1,z1),(x2,y2,z2))<=dist:
                body2.addForce(neg3(l))
                body1.addForce(l)

def explode():
    global bodies

    for b in bodies:
        l=list (b.getPosition ())
        scalp (l, (1000 / len3 (l))*pow(10,-11))
        b.addForce(l)

def brown(T,dt,body):
    imp=sqrt(6*N*log(10)*Ma*k*T)
    impfor=(2*(random()-0.5)*imp/dt,2*(random()-0.5)*imp/dt,2*(random()-0.5)*imp/dt)
    body.addForce(impfor)
    

def euler2mat(x,y,z):
    cx=cos(x)
    sx=sin(x)
    cy=cos(y)
    sy=sin(y)
    cz=cos(z)
    sz=sin(z)
    m00 = cx*cy
    m01 = sx*sz-cx*sy*cz
    m02 = cx*sy*sz+sx*cz
    m10 = sy
    m11 = cy*cz
    m12 = -cy*sz
    m20 = -sx*cy
    m21 = sx*sy*cz+cx*sz
    m22 = -sx*sy*sz+cx*cz
    return [[m00,m01,m02],[m10,m11,m12],[m20,m21,m22]]

def nint(floatpart):
    if floatpart-int(floatpart)<=0.5:
        nearest=int(floatpart)
    if floatpart-int(floatpart)>0.5:
        nearest=int(floatpart)+1
    return nearest

def alphaf(V,W,Z):
    if len3(cross(V,W))*len3(cross(W,Z))==0:
        alpha=pi/2
    else: 
        alpha=atan2(sinusalpha(V,W,Z),cosinusalpha(V,W,Z))
    return alpha

def signum(num):
    if num<0:
        return -1
    if num==0:
        return 0
    if num>0:
        return 1

def cosinusalpha(V,W,Z):
    cosalpha=(dot3(V,Z)-dot3(V,W)*dot3(W,Z))/(len3(cross(V,W))*len3(cross(V,W)))
    return cosalpha

def sinusalpha(V,W,Z):
    sinalpha=len3(cross(cross(W,V),cross(W,Z)))/(len3(cross(V,W)*len(cross(W,Z))))
    return sinalpha

def surf(V,W,Z):
    surf=alphaf(V,W,Z)+alphaf(W,Z,V)+alphaf(Z,V,W)-pi
    return surf

def sigmafunc(V,W,Z):
    sigma=signum(dot3(cross(V,W),Z))
    return sigma

def S3V(V,W,Z):
    s3v=(sigma(V,W,Z)*surf(V,W,Z))
    return s3v

def Wr3v(V,W,Z):
    if len3(cross(V,W))*len3(cross(W,Z))*len3(cross(Z,V))<pow(10,-10):
        wr3v=0
    else:
        wr3v=1/(2.*pi)*S3V(V,W,Z)
    return wr3v

def rotaxis(m):
    angle=acos((m[0]+m[4]+m[8]-1)/2)
    x=mat[7]-mat[5]/sqrt((mat[7]-mat[5])**2+(mat[2]-mat[6])**2+(mat[3]-mat[1])**2)
    y=mat[2]-mat[6]/sqrt((mat[7]-mat[5])**2+(mat[2]-mat[6])**2+(mat[3]-mat[1])**2)
    z=mat[3]-mat[1]/sqrt((mat[7]-mat[5])**2+(mat[2]-mat[6])**2+(mat[3]-mat[1])**2)
    omega=(x,y,z,angle)
    return omega

def vecU(v,m):
    diru=cross(v,m)
    normu=len3(cross(v,m))
    u=(diru[0]/normu,diru[1]/normu,diru[2]/normu)
    return u

def torad(deg):
    rad=deg*pi/180
    return rad

def todeg(rad):
    deg=rad*180/pi
    return deg

def transtuple(ODEtuplematrix):
    tuplematrixtranspose=(ODEtuplematrix[0],ODEtuplematrix[3],ODEtuplematrix[6],ODEtuplematrix[1],ODEtuplematrix[4],ODEtuplematrix[5],ODEtuplematrix[2],ODEtuplematrix[7],ODEtuplematrix[8])
    return tuplematrixtranspose

def sign(x):
    """Returns 1.0 if x is positive, -1.0 if x is negative or zero."""
    if x > 0.0: return 1.0
    else: return -1.0

def len3(v):
    """Returns the length of 3-vector v."""
    return sqrt(v[0]**2 + v[1]**2 + v[2]**2)

def neg3(v):
    """Returns the negation of 3-vector v."""
    return (-v[0], -v[1], -v[2])

def add3(a, b):
    """Returns the sum of 3-vectors a and b."""
    return (a[0] + b[0], a[1] + b[1], a[2] + b[2])

def sub3(a, b):
    """Returns the difference between 3-vectors a and b."""
    return (a[0] - b[0], a[1] - b[1], a[2] - b[2])

def mul3(v, s):
    """Returns 3-vector v multiplied by scalar s."""
    return (v[0] * s, v[1] * s, v[2] * s)

def div3(v, s):
    """Returns 3-vector v divided by scalar s."""
    return (v[0] / s, v[1] / s, v[2] / s)

def dist3(a, b):
    """Returns the distance between point 3-vectors a and b."""
    return len3(sub3(a, b))

def norm3(v):
    """Returns the unit length 3-vector parallel to 3-vector v."""
    l = len3(v)
    if (l > 0.0): return (v[0] / l, v[1] / l, v[2] / l)
    else: return (0.0, 0.0, 0.0)

def dot3(a, b):
    """Returns the dot product of 3-vectors a and b."""
    return (a[0] * b[0] + a[1] * b[1] + a[2] * b[2])

def cross(a, b):
    """Returns the cross product of 3-vectors a and b."""
    return (a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2],
        a[0] * b[1] - a[1] * b[0])

def project3(v, d):
    """Returns projection of 3-vector v onto unit 3-vector d."""
    return mul3(v, dot3(norm3(v), d))

def acosdot3(a, b):
    """Returns the angle between unit 3-vectors a and b."""
    x = dot3(a, b)
    if x < -1.0: return pi
    elif x > 1.0: return 0.0
    else: return acos(x)

def rotate3(m, v):
    """Returns the rotation of 3-vector v by 3x3 (row major) matrix m."""
    return (v[0] * m[0] + v[1] * m[1] + v[2] * m[2],
        v[0] * m[3] + v[1] * m[4] + v[2] * m[5],
        v[0] * m[6] + v[1] * m[7] + v[2] * m[8])

def invert3x3(m):
    """Returns the inversion (transpose) of 3x3 rotation matrix m."""
    return (m[0], m[3], m[6], m[1], m[4], m[7], m[2], m[5], m[8])

def xaxis(m):
    """Returns the z-axis vector from 3x3 (row major) rotation matrix m."""
    return (m[0], m[3], m[6])

def yaxis(m):
    """Returns the z-axis vector from 3x3 (row major) rotation matrix m."""
    return (m[1], m[4], m[7])

def zaxis(m):
    """Returns the z-axis vector from 3x3 (row major) rotation matrix m."""
    return (m[2], m[5], m[8])

def calcRotMatrix(axis, angle):
    """
    Returns the row-major 3x3 rotation matrix defining a rotation around axis by
    angle.
    """
    cosTheta = cos(angle)
    sinTheta = sin(angle)
    t = 1.0 - cosTheta
    return (
        t * axis[0]**2 + cosTheta,
        t * axis[0] * axis[1] - sinTheta * axis[2],
        t * axis[0] * axis[2] + sinTheta * axis[1],
        t * axis[0] * axis[1] + sinTheta * axis[2],
        t * axis[1]**2 + cosTheta,
        t * axis[1] * axis[2] - sinTheta * axis[0],
        t * axis[0] * axis[2] - sinTheta * axis[1],
        t * axis[1] * axis[2] + sinTheta * axis[0],
        t * axis[2]**2 + cosTheta)

def makeOpenGLMatrix(r, p):
    """
    Returns an OpenGL compatible (column-major, 4x4 homogeneous) transformation
    matrix from ODE compatible (row-major, 3x3) rotation matrix r and position
    vector p.
    """
    return (
        r[0], r[3], r[6], 0.0,
        r[1], r[4], r[7], 0.0,
        r[2], r[5], r[8], 0.0,
        p[0], p[1], p[2], 1.0)

def getBodyRelVec(b, v):
    """
    Returns the 3-vector v transformed into the local coordinate system of ODE
    body b.
    """
    return rotate3(invert3x3(b.getRotation()), v)

def near_callback(args,geom1,geom2):
        if(ode.areConnected(geom1.getBody(),geom2.getBody())):
                return
        contacts=ode.collide(geom1,geom2)
        world,contactgroup=args
#   if contacts!=[]:
#         print len(contacts)
        for c in contacts:
                c.setBounce(0)
                c.setMu(000)
                j=ode.ContactJoint(world,contactgroup,c)
                j.attach(geom1.getBody(),geom2.getBody())
                #intfreq.append(((bases[bodies.index(c.getContactGeomParams()[-1].getBody())].name,bases[bodies.index(c.getContactGeomParams()[-2].getBody())].name)))
                #if nucleus1 in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]] or nucleus2 in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]] or colcyl in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]]:
                #        pass
                #else:
                        #num1=bodies.index(c.getContactGeomParams()[-1].getBody())
                        #num2=bodies.index(c.getContactGeomParams()[-2].getBody())
                        #intfreq.append((num1,num2,total_time/dt))
                        #colmatrix[num1,num2]+=1
                        #colmatrix[num2,num1]+=1

def near_callback_register(args,geom1,geom2):
    if(ode.areConnected(geom1.getBody(),geom2.getBody())):
        return
    contacts=ode.collide(geom1,geom2)
    world,contactgroup=args
#   if contacts!=[]:
#     print len(contacts)
    for c in contacts:
        c.setBounce(0)
        c.setMu(000)
        j=ode.ContactJoint(world,contactgroup,c)
        j.attach(geom1.getBody(),geom2.getBody())
        #intfreq.append(((bases[bodies.index(c.getContactGeomParams()[-1].getBody())].name,bases[bodies.index(c.getContactGeomParams()[-2].getBody())].name)))
        if Nucleus and (nucleus1 in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]] or nucleus2 in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]] or colcyl in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]]):
            pass
        else:
            num1=bodies.index(c.getContactGeomParams()[-1].getBody())
            num2=bodies.index(c.getContactGeomParams()[-2].getBody())
            intfreq.append((int(num1),int(num2),int(total_time/dt)))
            colmatrix[num1,num2]+=1
            colmatrix[num2,num1]+=1

def near_callback_register2(args,geom1,geom2):
        if(ode.areConnected(geom1.getBody(),geom2.getBody())):
                return
        contacts=ode.collide(geom1,geom2)
        world,contactgroup=args
#   if contacts!=[]:
#         print len(contacts)
        for c in contacts:
                c.setBounce(0)
                c.setMu(000)
                j=ode.ContactJoint(world,contactgroup,c)
                j.attach(geom1.getBody(),geom2.getBody())
                #intfreq.append(((bases[bodies.index(c.getContactGeomParams()[-1].getBody())].name,bases[bodies.index(c.getContactGeomParams()[-2].getBody())].name)))
                if Nucleus and (nucleus1 in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]] or nucleus2 in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]] or colcyl in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]]):
                        pass
                else:
                        num1=bodies.index(c.getContactGeomParams()[-1].getBody())
                        num2=bodies.index(c.getContactGeomParams()[-2].getBody())
                        intfreq.append((num1,num2,total_time/dt))
                        colmatrix2[num1,num2]+=1
                        colmatrix2[num2,num1]+=1

def near_callback_topo(args,geom1,geom2):
    if(ode.areConnected(geom1.getBody(),geom2.getBody())):
        return
    if Nucleus and (nucleus1 in [geom1,geom2] or nucleus2 in [geom1,geom2] or colcyl in [geom1,geom2]):
        contacts=ode.collide(geom1,geom2)
        world,contactgroup=args
    else:
        return
#   if contacts!=[]:
#         print len(contacts)
        for c in contacts:
            c.setBounce(0)
            c.setMu(000)
            j=ode.ContactJoint(world,contactgroup,c)
            j.attach(geom1.getBody(),geom2.getBody())
                #intfreq.append(((bases[bodies.index(c.getContactGeomParams()[-1].getBody())].name,bases[bodies.index(c.getContactGeomParams()[-2].getBody())].name)))
                #if nucleus1 in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]] or nucleus2 in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]] or colcyl in [c.getContactGeomParams()[-2],c.getContactGeomParams()[-1]]:
                        #pass
                #else:
                        #num1=bodies.index(c.getContactGeomParams()[-1].getBody())
                        #num2=bodies.index(c.getContactGeomParams()[-2].getBody())
                        #intfreq.append((num1,num2,total_time/dt))
                        #colmatrix[num1,num2]+=1
                        #colmatrix[num2,num1]+=1


def maketube(verts,diameter,length,name):
    mesh=Mesh.Primitives.Tube(verts,diameter,length)
    scn=Scene.GetCurrent()
    ob=scn.objects.new(mesh,name)
    return ob

def makecube(x,y,z,name):
    vertices_list=[ 
        [-0.5*x,-0.5*y,-0.5*z],
        [-0.5*x,0.5*y,-0.5*z],
        [0.5*x,0.5*y,-0.5*z],
        [0.5*x,-0.5*y,-0.5*z],
        [-0.5*x,-0.5*y,0.5*z],
        [-0.5*x,0.5*y,0.5*z],
        [0.5*x,0.5*y,0.5*z],
        [0.5*x,-0.5*y,0.5*z]
    ]
    
    faces_list=[[0,1,2,3],
        [7,6,5,4],
        [3,7,4,0],
        [5,6,2,1],
        [4,5,1,0],
        [2,6,7,3]]
    
    A_CUBE_MESH=NMesh.GetRaw()
    
    for coordinate in vertices_list:
        A_VERTEX=NMesh.Vert(coordinate[0], coordinate[1], coordinate[2])
        A_CUBE_MESH.verts.append(A_VERTEX)
    
    for thisface in faces_list:
        A_FACE=NMesh.Face()
        for vertexpos in thisface:
            A_FACE.append(A_CUBE_MESH.verts[vertexpos])
        A_CUBE_MESH.faces.append(A_FACE)
    
    
    scn=Scene.GetCurrent()
    ob=scn.objects.new(A_CUBE_MESH,name)
    
    return ob

# the update function
def update():
    for j in links:
        baseAxis=zaxis(j.getBody(0).getRotation())
        currAxis=zaxis(j.getBody(1).getRotation())
        angle=acosdot3(currAxis,baseAxis)
        relAngVel=sub3(
        j.getBody(1).getAngularVel(),
        j.getBody(0).getAngularVel())
        twistAngVel=project3(relAngVel,currAxis)
        flexAngVel=sub3(relAngVel, twistAngVel)
        flexLimit=pi/16
        flexForce=1*pow(10,-18)
        if angle > flexLimit:
            j.getBody(0).addTorque(mul3(
            norm3(cross(currAxis, baseAxis)),
            (angle - flexLimit) * flexForce))

def getorientation(blendobj):
    content=blendobj.split()
    aa=float(content[6]) # changed from a list starting from content[8] that do not make logical sense
    ab=float(content[7])
    ac=float(content[8])
    ba=float(content[9])
    bb=float(content[10])
    bc=float(content[11])
    ca=float(content[12])
    cb=float(content[13])
    cc=float(content[14])
    return (aa,ab,ac,ba,bb,bc,ca,cb,cc)

def bodysize(blendobj):
    content=blendobj.split()
    sizex=float(content[18])
    sizey=float(content[19])
    sizez=float(content[20])
#   originalrot=blendobj.getMatrix().copy()
#   originalloc=blendobj.getLocation()
#   zerorot=Euler((0,0,0)).toMatrix()
#   zeroloc=Euler((0,0,0))
#   blendobj.setMatrix(zerorot)
##  blendobj.setLocation(zeroloc)
#   blendobj.LocX=zeroloc[0]
#   blendobj.LocY=zeroloc[1]
#   blendobj.LocZ=zeroloc[2]
#   bbox=blendobj.boundingBox
#   sizez=abs(bbox[0][2]-bbox[6][2])
#   sizey=abs(bbox[0][1]-bbox[6][1])
#   sizex=abs(bbox[0][0]-bbox[6][0])
##  blendobj.setLocation(originalloc)
#   blendobj.LocX=originalloc[0]
#   blendobj.LocY=originalloc[1]
#   blendobj.LocZ=originalloc[2]
#   blendobj.setMatrix(originalrot)
    return (sizex,sizey,sizez)

def odeCylBody(blendobj,world,space):
    body=ode.Body(world)
    body.setMass(M)
    coords=blendobj.split()
    body.setPosition((float(coords[3]),float(coords[4]),float(coords[5])))
#   body.setPosition((blendobj.getLocation()[0]*1e-8,blendobj.getLocation()[1]*1e-8,blendobj.getLocation()[2]*1e-8))
#blender specific
#   rot=blendobj.getMatrix().copy().transpose() 
    rot=getorientation(blendobj)
    body.setRotation((\
    rot[0],rot[1],rot[2],\
    rot[3],rot[4],rot[5],\
    rot[6],rot[7],rot[8]))
    return body

def odeCylGeom(blendobj,body,space):
    size=bodysize(blendobj)
    radius=0.5*(size[0])*1e-8
    Lz=(size[2])*1e-8
    geom=ode.GeomCapsule(space,radius,Lz)
#blender specific
#   rot=blendobj.getMatrix().copy().transpose()
#    print "working..."
    rot=getorientation(blendobj)
    geom.setRotation((\
        rot[0],rot[1],rot[2],\
        rot[3],rot[4],rot[5],\
        rot[6],rot[7],rot[8]))
    geom.setBody(body)
    return geom

#blender specific
#def odeCylMesh(blendobj,body,name):
#   size=bodysize(blendobj)
#   rot=blendobj.getMatrix().copy().transpose() 
#   radius=(0.5*size[0])
#   Lz=(size[2])
#   mesh=maketube(verts,2*radius,Lz,name)
#   mesh.setMatrix(rot)
##  mesh.setLocation(body.getPosition())
#   mesh.LocX=body.getPosition()[0]
#   mesh.LocY=body.getPosition()[1]
#   mesh.LocZ=body.getPosition()[2]
#   return mesh
#

def getByPrefix(pre,objects):
    prefixlist=[]
    for ob in objects:
        prefix=ob.split(".")[0]
        if prefix==pre:
            prefixlist.append(ob)
        else: pass
    return prefixlist

def getAnchor(klist):
    Kjoints=[]
    for j in range(len(klist)-1):
#blender specific
#     x1,y1,z1=klist[j].getLocation()
#     x2,y2,z2=klist[j+1].getLocation()
        coord1=klist[j].split()
        coord2=klist[j+1].split()
        x1,y1,z1=float(coord1[3]),float(coord1[4]),float(coord1[5])
        x2,y2,z2=float(coord2[3]),float(coord2[4]),float(coord2[5])
#     print x1,x2,y1,y2,z1,z2
        anchorpos=((x1+(x2-x1)/2.,y1+(y2-y1)/2.,z1+(z2-z1)/2.))
        Kjoints.append(anchorpos)
    return Kjoints

def getAnchor_from_joint_list(jlist):
    Kjoints=[]
    for j in range(len(jlist)):
        coords=jlist[j].split()
        x,y,z=(float(coords[1]),float(coords[2]),float(coords[3]))
        anchorpos=(x,y,z)
        Kjoints.append(anchorpos)
    return Kjoints
    
def makeJoints(klist,kbodies,kjoints,limit):
    Klinks=[]
    for i in range(len(klist)-1):
        joint=ode.BallJoint(world)
        joint.attach(kbodies[i],kbodies[i+1])
#     joint.setAnchor((kjoints[i][0]*1e-8,kjoints[i][1]*1e-8,kjoints[i][2]*1e-8))
        joint.setAnchor((kjoints[i][0],kjoints[i][1],kjoints[i][2]))
        #rot=Euler(0,0,0).toMatrix().transpose()
        #joint.setAxis1((rot[0][0],rot[1][0],rot[2][0]))
        #joint.setAxis2((rot[0][1],rot[1][1],rot[2][1]))
        #joint.setParam(ode.ParamLoStop, torad(-limit))
        #joint.setParam(ode.ParamHiStop, torad(limit))
        #joint.setParam(ode.ParamLoStop2, torad(-limit))
        #joint.setParam(ode.ParamHiStop2, torad(limit))
        Klinks.append(joint)
    return Klinks
    
def makeIPO(object,name):
    Kelt=object
    Keltipo=Ipo.New("Object",name)
    posx=Keltipo.addCurve('LocX')
    posy=Keltipo.addCurve('LocY')
    posz=Keltipo.addCurve('LocZ')
    rotx=Keltipo.addCurve('RotX')
    roty=Keltipo.addCurve('RotY')
    rotz=Keltipo.addCurve('RotZ')
    posx.setInterpolation('Linear')
    posx.setExtrapolation('Constant')
    posy.setInterpolation('Linear')
    posy.setExtrapolation('Constant')
    posz.setInterpolation('Linear')
    posz.setExtrapolation('Constant')
    Kelt.setIpo(Keltipo)

world=ode.World()
world.setERP(0.8)
world.setCFM(10)
space=ode.Space(1)
space.setLevels(-22,-21)
contactgroup=ode.JointGroup()

bases=[]
bodies=[]
links=[]
geoms=[]
init=0
euX,euY,euZ=0,0,0

def describeK(klist):
    joints=[]
    for k in range(len(klist)-1):
        x1,y1,z1=klist[k].getLocation()
        x2,y2,z2=klist[k+1].getLocation()
        anchorpos=((x1+(x2-x1)/2.,y1+(y2-y1)/2.,z1+(z2-z1)/2.))
        joints.append(anchorpos)

#returns a list of all the objects in the current scene
#blender specific
#objects=Blender.Object.Get()

#-----------------------describe the system-----------------------
# essential files 
d = os.path.dirname(argv[2])

if not os.path.exists(d):
    os.makedirs(d)
elif os.path.exists(d):
    pass

sourcepath="/Users/hwong/simurep/"
simpath="/Users/hwong/simurep/"

#initco=file(sourcepath+'InitCoordLongestK12','r')
initco=file(argv[1],'r')
#diagnose=file(simpath+'diagnosis/diagnosis'+host+str(pidnum)+time.asctime().replace(' ','_')+str(stickiness),"w")
diagnose=file(argv[2]+'diagnosis','w')
#interactions=file(simpath+'3Cinteractions/3C'+host+str(pidnum)+time.asctime().replace(' ','_')+str(stickiness),'w')
interactions=file((argv[2]+'3C'),'w')
#Rescue=file(simpath+'trajectories/test'+host+str(pidnum)+time.asctime().replace(' ','_')+str(stickiness),"w")
Rescue=file((argv[2]+'trajectory'),'w')

if len(argv)==7:
        lecco=initco.readlines()
if len(argv)>7:
        lecco=initco.readlines()
        initjo=file(argv[7])
        lecjo=initjo.readlines()

matrixsize=len(lecco)
colmatrix=zeros([matrixsize,matrixsize])
colmatrix2=zeros([matrixsize,matrixsize])

Kbodies=[]
Kbases=[]

##---------describe the nucleus collision mesh
#blender specific
#nucleus1=TrimeshCollision('nucleus1',0,world,space)
#nucleus2=TrimeshCollision('nucleus2a',0,world,space)
#nucleolus=TrimeshCollision('nucleolus',0,world,space)
#
#
##---------describe the SPB collision mesh
#spb=TrimeshCollision('SPB',1,world,space)

# move to parameter files later on
#if no parameters given, use default nucleus obj
(VERTNUC1,INDNUC1)=obj2tri(RepNoyau + "sphtop_wall.obj",-1)
(VERTNUC2,INDNUC2)=obj2tri(RepNoyau + "sphbot_wall.obj",-1)
(VERTCYL,INDCYL)=obj2tri(RepNoyau + "cyl_wall.obj",-1)

#print VERTCYL
#print INDCYL

vertnucSC1=list(VERTNUC1)
vertnucSC2=list(VERTNUC2)
vertcylSC=list(VERTCYL)

for el in range(len(VERTNUC1)):
    vertnucSC1[el]=list(VERTNUC1[el])

for el in range(len(VERTNUC2)):
    vertnucSC2[el]=list(VERTNUC2[el])

for el in range(len(VERTCYL)):
    vertcylSC[el]=list(VERTCYL[el])

#scalenuc=0.9
scalenuc=float(argv[4])/2.

avertnucSC1=array(vertnucSC1)*scalenuc
avertnucSC2=array(vertnucSC2)*scalenuc
avertcylSC=multiply(array(vertcylSC),[scalenuc,scalenuc,1])

if Nucleus:
    #print (avertnucSC1) , len(vertnucSC1) ,max(INDNUC1) , len(INDNUC1)
    nuc1=ode.TriMeshData()
    nuc1.build(avertnucSC1,INDNUC1)
    nucleus1=ode.GeomTriMesh(nuc1,space)
    
    nuc2=ode.TriMeshData()
    nuc2.build(avertnucSC2,INDNUC2)
    nucleus2=ode.GeomTriMesh(nuc2,space)
    
    cyl=ode.TriMeshData()
    cyl.build(avertcylSC,INDCYL)
    colcyl=ode.GeomTriMesh(cyl,space)

limit=120
verts=6


#----- global list with all chromosomes
Klist=[]
#----- global list with all microtubules
if microtubule:
    MTlist=[]

lenlist=[]
#----- global list with all centromeres
#cen=array([30,48,22,90,30,30,100,21,71,88,88,30,53,126,65,111])

#param=file("/home1/Imod/hwong/scripts/massive/parm")
param=file(argv[5],'r')
cenlist=param.readlines()
iter=0
for i in cenlist:
    i=atoi(i.strip())
    cenlist[iter]=i
    iter+=1

cen=array(cenlist)

#cen=cen/2 #---- LP segment centromere position modificator Comment to put back to LP30nm
cen=cen-1

print "cen",cen

#Creation chromosomes
for c in range(1, n_chromosomes + 1):
    if len(argv)==7:
        locals()['K%i' % c] = getByPrefix('k%i' % c,lecco)
        locals()['MT%i' % c] = getByPrefix('MT%i' % c,lecco)
        locals()['K%ijoints' % c] = getAnchor(locals()['K%i' % c])
    if len(argv)>7:
        locals()['K%i' % c] = getByPrefix('k%i' % c,lecco)
        locals()['MT%i' % c] = getByPrefix('MT%i' % c,lecco)
        locals()['J%i' % c] = getByPrefix('k%i' % c,lecjo)
        locals()['K%ijoints' % c] = getAnchor_from_joint_list(locals()['J%i' % c])
    
    locals()['K%ibodies' % c] = []
    locals()['K%igeoms' % c] = [] 
    for i in range(len(locals()['K%i' % c])):
        locals()['K%ibody' % c] = odeCylBody(locals()['K%i' % c][i],world,space)
        locals()['K%igeom' % c] = odeCylGeom(locals()['K%i' % c][i],locals()['K%ibody' % c],space)
        locals()['K%ibodies' % c] .append(locals()['K%ibody' % c])
        locals()['K%igeoms' % c] .append(locals()['K%igeom' % c] )
        Kbodies .append(locals()['K%ibody' % c])
    
    Klist.append(locals()['K%ibodies' % c] )
    lenlist.append(len(locals()['K%i' % c]))
    
    locals()['K%ilinks' % c] = makeJoints(locals()['K%i' % c],
                                locals()['K%ibodies' % c],
                                locals()['K%ijoints' % c],limit)
                                
#    if microtubule:
#        print "doing MTs"
#        locals()['MT%ibody' % c] = odeCylBody(locals()['MT%i' % c][0],world,space)
#        locals()['MT%igeom' % c] = odeCylGeom(locals()['MT%i' % c][0],locals()['MT%ibody' % c],space)
        
    
        #K1bodies.append(MT1body)
#        locals()['K%igeoms' % c].append(locals()['MT%igeom' % c])
#        Kbodies.append(locals()['MT%ibody' % c])
        
#        MTlist.append([locals()['MT%ibody' % c]])
        
#        locals()['MT%iJ1' % c]=ode.BallJoint(world)
#        locals()['MT%iJ1' % c].attach(locals()['MT%ibody' % c],locals()['K%ibodies' % c] [cen[c - 1]])
#        locals()['MT%iJ1' % c].setAnchor((locals()['K%ibodies' % c] [cen[c - 1]].getPosition()[0],
#                                locals()['K%ibodies' % c] [cen[c - 1]].getPosition()[1],
#                                locals()['K%ibodies' % c] [cen[c - 1]].getPosition()[2]))
#        print "toto",locals()['K%ibodies' % c] [cen[c - 1]].getPosition()
#        locals()['MT%iJ2' % c]=ode.BallJoint(world)
#        locals()['MT%iJ2' % c].attach(locals()['MT%ibody' % c],None)
#        locals()['MT%iJ2' % c].setAnchor((0,0,91.4*1e-8))
    
#    print "K%i done" % c



#---------describe chromosome 1
#K1=getByPrefix('k1',lecco)
#MT1=getByPrefix('MT1',lecco)
#K1joints=getAnchor(K1)
#
#K1bodies=[]
#K1geoms=[]
#for i in range(len(K1)):
#   K1body=odeCylBody(K1[i],world,space)
#   K1geom=odeCylGeom(K1[i],K1body,space)
#   K1bodies.append(K1body)
#   K1geoms.append(K1geom)
#   Kbodies.append(K1body)
#
#Klist.append(K1bodies)
#lenlist.append(len(K1))
#
#K1links=makeJoints(K1,K1bodies,K1joints,limit)
#
#MT1body=odeCylBody(MT1[0],world,space)
#MT1geom=odeCylGeom(MT1[0],MT1body,space)
##K1bodies.append(MT1body)
#K1geoms.append(MT1geom)
#Kbodies.append(MT1body)
#
#MTlist.append([MT1body])
#
#MT1J1=ode.BallJoint(world)
#MT1J1.attach(MT1body,K1bodies[cen[0]])
#MT1J1.setAnchor((K1bodies[cen[0]].getPosition()[0],K1bodies[cen[0]].getPosition()[1],K1bodies[cen[0]].getPosition()[2]))
#MT1J2=ode.BallJoint(world)
#MT1J2.attach(MT1body,None)
#MT1J2.setAnchor((0,0,91.4*1e-8))
#
#print "K1 done"


#Do the simulation...
bases=Kbases
bodies=Kbodies

total_time=0.

##sticky collisions parameters

for c in range(1, n_chromosomes + 1):
    locals()['contactgroup%iL' % c] = []
    locals()['contactgroup%iR' % c] = []
#contactgroup1L=[]
#contactgroup2L=[]
#...16
#
#contactgroup1R=[]
#contactgroup2R=[]
#...16

TL=[]
TR=[]

for c in range(1, n_chromosomes + 1):
    TL.append((locals()['K%igeoms' % c][0], locals()['K%ibodies' % c][0]))
    TR.append((locals()['K%igeoms' % c][-2], locals()['K%ibodies' % c][-1]))

#TL.append((K1geoms[0],K1bodies[0]))
#TR.append((K1geoms[-2],K1bodies[-1]))
#



pullzone=(98.08*1e-8,0.*1e-8,19.51*1e-8)

recover=int(argv[3])

if recover==0:
    if Nucleus:
        if len(argv)==7:
            nucleus1.setPosition((0,0,1300*1e-8))
            nucleus2.setPosition((0,0,-7000*1e-8))
        if len(argv)>7:
            nucleus1.setPosition((0,0,0))
            nucleus2.setPosition((0,0,0))

if recover==1:
    if Nucleus:
        nucleus1.setPosition((0,0,0))
        nucleus2.setPosition((0,0,0))
#   nucleolus.setPosition((0,0,0))  
    #Revive=file(sourcepath+'traj13step9000000',"r")
    revint=randint(1,200)
    Revive=file('/home1/Imod/hwong/lazaruspit/revive'+str(revint)+'.csv',"r") 
#   Rescue=file('/pasteur/simunoyau/trajectories/test',"w")
    Data=[]
    Dataread=Revive.readline()
    Data.append(Dataread)
    while Dataread!="":
        Dataread=Revive.readline()
        Data.append(Dataread)
    Data.pop()
#Consume too much memory
#   Data=Revive.readlines()
    Revive.close()
    store=[]
    leng=len(bodies)
    while leng!=0:
        store.append(Data[-leng].split('\t'))
        leng=leng-1
    for b in range(len(bodies)):
        x,y,z=float(store[b][2]),float(store[b][3]),float(store[b][4])
        matr=(float(store[b][5]),float(store[b][6]),float(store[b][7]),\
        float(store[b][8]),float(store[b][9]),float(store[b][10]),\
        float(store[b][11]),float(store[b][12]),float(store[b][13]))
        vx,vy,vz=float(store[b][9]),float(store[b][10]),float(store[b][11])
        bodies[b].setPosition((x,y,z))
        bodies[b].setRotation(matr)
        bodies[b].setLinearVel((vx,vy,vz))

Klen=[]
for k in range(len(lenlist)):
    #print lenlist
    Klen.append(lenlist[k])
    #print Klen
Klen.sort()
#print lenlist
#modif

breaking_bad=0

#duration=1e-8
while total_time<=duration:
#    print total_time/dt , duration/dt , (duration/dt)/100., (total_time/dt)>(duration/dt)/100.
    if (total_time/dt)>(duration/dt)/100. and breaking_bad==0:
        b4=K4links[107].getBody(0)
        print b4
        K4links[107].attach(None,None)
        aft=K4links[107].getBody(0)
        print aft
        breaking_bad=1
    if int((total_time/dt)%directorysave)==0:
        print total_time/dt , duration/dt , (duration/dt)/100., (total_time/dt)>(duration/dt)/100.
        directoryname=str(initdirectory)+'_'+str(initdirectory+directorysave)
        initdirectory+=directorysave
    coord=[]
    vel=[]
    pos2=[]
    rotat=[]
    steps=[]
    if int((total_time/dt)%topostep)==0:
        space.collide((world,contactgroup),near_callback_topo)
    elif int((total_time/dt)%sampling)==0:
        space.collide((world,contactgroup),near_callback_register) 
    else:
        space.collide((world,contactgroup),near_callback_register2) #test position

### test code for sticky collisions
    if Nucleus:
        for nuclei in range(1,3):
            for group in range(1,n_chromosomes + 1):
                stickycol(TR[group - 1][0],locals()['nucleus%i'%nuclei],
                                 locals()['contactgroup%iR'%group],
                                stickiness,'%ir'%group)
                stickycol(TL[group - 1][0],locals()['nucleus%i'%nuclei],
                                 locals()['contactgroup%iL'%group],
                                stickiness,'%il'%group)
                                
    
            
#   stickycol(TR[0][0],nucleus1,contactgroup1R,stickiness,'1r')
#   stickycol(TL[0][0],nucleus1,contactgroup1L,stickiness,'1l')
#   stickycol(TR[1][0],nucleus1,contactgroup2R,stickiness,'2r')
#   stickycol(TL[1][0],nucleus1,contactgroup2L,stickiness,'2l')
#   ... 15
#
#   stickycol(TR[0][0],nucleus2,contactgroup1R,stickiness,'1r')
#   stickycol(TL[0][0],nucleus2,contactgroup1L,stickiness,'1l')
#   stickycol(TR[1][0],nucleus2,contactgroup2R,stickiness,'2r')
#   stickycol(TL[1][0],nucleus2,contactgroup2L,stickiness,'2l')
#   ... 15

    for b in TL:
        l=list (b[1].getPosition ())
        nl=list(norm3(l))
        scalp (nl, 40000*pow(10,-10))
        d=dist3(l,(0,0,0))
        if d<1e-6:
            b[1].addForce(nl)

    for b in TR:
        l=list (b[1].getPosition ())
        nl=list(norm3(l))
        scalp (nl, 40000*pow(10,-10))
        if d<1e-6:
            b[1].addForce(nl)

    for b in range(len(bodies)):
        langevin_tr(bodies[b],sigma,friction)
#       x,y,z=bodies[b].getPosition()
#       vx,vy,vz=bodies[b].getLinearVel()
#       vel.append((vx,vy,vz))
#       pos2.append((x,y,z))
#       aa,ab,ac,ba,bb,bc,ca,cb,cc=bodies[b].getRotation()
#       mat=matrix([[aa,ab,ac,0],[ba,bb,bc,0],[ca,cb,cc,0],[x,y,z,1.0]])
#       mat=mat.transpose()

###########init gzip stuff now
#   gzipcdf=gzip.open(filename,'wb')

###########netcdf stuff
    if int((total_time/dt)%sampling)==0:
        st=total_time/dt
###########init gzip stuff now
        if st!=0 and st%1==0.:
            st=st-1
        if not os.path.exists(argv[2]+'/step'+directoryname):
            os.makedirs(argv[2]+'/step'+directoryname)
        elif os.path.exists(argv[2]+'/step'+directoryname):
            pass
        filename=argv[2]+'/step'+directoryname+'/traj.nc'+str("%d"%(int(st)))
        gzipcdf=gzip.open(filename+'.gz','wb')      
        ncfile=netcdf.netcdf_file(filename,'w')
        coordsdatak=[]
        for it in range(n_chromosomes):
            coordsdata=[]
            ###init variables
            xdata=zeros([Klen[-1]])
            xdata[:]=NAN
            ydata=zeros([Klen[-1]])
            ydata[:]=NAN
            zdata=zeros([Klen[-1]])
            zdata[:]=NAN
            aa=zeros([Klen[-1]])
            aa[:]=NAN
            ab=zeros([Klen[-1]])
            ab[:]=NAN
            ac=zeros([Klen[-1]])
            ac[:]=NAN
            ba=zeros([Klen[-1]])
            ba[:]=NAN
            bb=zeros([Klen[-1]])
            bb[:]=NAN
            bc=zeros([Klen[-1]])
            bc[:]=NAN
            ca=zeros([Klen[-1]])
            ca[:]=NAN
            cb=zeros([Klen[-1]])
            cb[:]=NAN
            cc=zeros([Klen[-1]])
            cc[:]=NAN
            for bod in range(len(Klist[it])):
                posxyz=Klist[it][bod].getPosition()
                x=posxyz[0]
                y=posxyz[1]
                z=posxyz[2]
                xdata[bod]=x
                ydata[bod]=y
                zdata[bod]=z
                rotxyz=Klist[it][bod].getRotation()
                rotaa=rotxyz[0]
                rotab=rotxyz[1]
                rotac=rotxyz[2]
                rotba=rotxyz[3]
                rotbb=rotxyz[4]
                rotbc=rotxyz[5]
                rotca=rotxyz[6]
                rotcb=rotxyz[7]
                rotcc=rotxyz[8]
                aa[bod]=rotaa
                ab[bod]=rotab
                ac[bod]=rotac
                ba[bod]=rotba
                bb[bod]=rotbb
                bc[bod]=rotbc
                ca[bod]=rotca
                cb[bod]=rotcb
                cc[bod]=rotcc
            #for bod in Klist[it]:
            #   posxyz=bod.getPosition()
            #   x=posxyz[0]
            #   y=posxyz[1]
            #   z=posxyz[2]
            #   print bod
            #   xdata[bod]=x
            #   ydata[bod]=y
            #   zdata[bod]=z
            coordsdata.append([xdata,ydata,zdata,aa,ab,ac,ba,bb,bc,ca,cb,cc])
            coordsdatak.append(coordsdata)
        ncfile.createDimension('chrlen',Klen[-1])
        ncfile.createDimension('xyzrot',12)
        ncfile.createDimension('chromosome',n_chromosomes)
        ncfile.createDimension('time',1)
        coords=ncfile.createVariable('coords','d',('chromosome','time','xyzrot','chrlen',))
#       print array(coordsdatak)    
        coords[:]=array(coordsdatak)
#       gzipcdf.write(ncfile)
        ncfile.close()
        netcdf_in=open(filename,'rb')
        gzipcdf.writelines(netcdf_in)
        netcdf_in.close()
        os.remove(filename)
        gzipcdf.close()

        intfreq=array(intfreq)
        intfilename=argv[2]+'/step'+directoryname+'/int3C'+str("%d"%(int(st)))
        gzipint=gzip.open(intfilename+'.gz','wb')
        savint=file(intfilename,'w')
        #savint.write(str(intfreq))
        savetxt(savint,intfreq,fmt='%6d')
        savint.close()
        int_in=open(intfilename,'rb')
        gzipint.writelines(int_in)
        int_in.close()
        os.remove(intfilename)
        gzipint.close()
        intfreq=[]  

###########xml stuff
    world.quickStep(dt)
    if Nucleus:
        if nucleus1.getPosition()[2]>=(1-scalenuc)*1e-6 and total_time/dt>300000:
            nucleus1.setPosition((0,0,nucleus1.getPosition()[2]-0.25*1e-9))
        if nucleus2.getPosition()[2]<=(1-scalenuc)*1e-6:
            nucleus2.setPosition((0,0,nucleus2.getPosition()[2]+0.25*1e-9))
    contactgroup.empty()
    total_time+=dt




Rescue.close()
