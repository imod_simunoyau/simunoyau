#    <This is a script that model nuclear interactions as systems of rigid bodies>
#    Copyright (C) 2008  Hua WONG <wong.hua____at__gmail__dot_com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
import math
import copy
import numpy as np
import gridpol8
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import collections


## include in a text file later ?
kuhn=60
segradius=25
rkuhn=3*kuhn
nucradius=rkuhn
compaction=5000 #(bp/60nm)

rdna={
#0:[[5000,20000],[5595000,200000]]
2:[[5000,750000],[3490000,750000]],
#1:[[10000,750000],[150000,750000],[3095000,750000],[3495000,750000]]
#1:[[10000,5000],[150000,50000],[3095000,500000],[3495000,10000]]
}

chrlen={
#0:5000000
0:5600000,
1:4500000,
2:3500000,

}

chrcen={
#0:20000
0:3780000,
1:1623000,
2:1136000,
}

# change chrcen and chrlen for rDNA insertion
insertion=0
for k in rdna.keys():
    print "k",k
    if k in rdna.keys() and k in chrlen.keys():
        for insert in range(len(rdna[k])):
            rdna[k][insert][0]=rdna[k][insert][0]+insertion
            chrlen[k]=chrlen[k]+rdna[k][insert][1]
            if rdna[k][insert][0]<=chrcen[k]:
                chrcen[k]=chrcen[k]+rdna[k][insert][1]
            print rdna[k][insert][0]<chrcen[k]
            insertion=insertion+rdna[k][insert][1]
            print "insertion",insertion

#mix the new configs to set polymer segments length
chrseg={}
for k in chrlen.keys():
    segnum=chrlen[k]/compaction
    chr=[]
#    segnum=chrlen[k]/compaction
#    chr=[]
    for seg in range(segnum):
        chr.append(kuhn)
    chrseg[k]=chr

#change relevant segments to rDNA
for k in chrseg:
    if k in rdna.keys():
        for insertions in rdna[k]:
            beg=insertions[0]/compaction
            end=(insertions[0]+insertions[1])/compaction
            print beg,end,k
            for seg in range(len(chrseg[k])):
#                print seg
                if seg in range(beg,end):
#                    print "replacing"
                    chrseg[k][seg]=rkuhn

#make an ordered python dictionary
#ochrlen=collections.OrderedDict(sorted(chrlen.items(), key=lambda t: t[1]))

radius=1500
occupied=[]

#pu_centromeres=gridpol8.circle(480,840,120) # known to converge after ~50 tries with a 1500nm nucleus
pu_centromeres=gridpol8.circle(300,840,60)
print len(pu_centromeres)

centromeres=set()
ranclist=[]
while len(centromeres)<len(chrlen):
    ranc=random.randint(0,len(pu_centromeres)-1)
    centromeres.update([pu_centromeres[ranc]])

centromeres=list(centromeres)
print len(centromeres)
occupied.extend(centromeres)

krstatus=[False]
klstatus=[False]

rarm=[]
larm=[]
rarm={}
larm={}
for i in range(len(chrlen)):
#    larm.append(chrcen[i])
#    rarm.append(chrlen[i]-chrcen[i])
    larm[i]=(chrcen[i])
    rarm[i]=(chrlen[i]-chrcen[i])

##divide chrseg in right and left arms
rchrseg={}
lchrseg={}
for k in chrseg.keys():
    lchrseg[k]=chrseg[k][0:chrcen[k]/compaction]
    lchrseg[k].reverse()
    rchrseg[k]=chrseg[k][chrcen[k]/compaction:chrlen[k]/compaction]

def gridpol(start,center,occupied,iterations,kuhnlist,stdkuhn,radius,exclusion_zone):
    ###test with draw because I am sick of not knowing what the #$$% is going on
    coords=[]
    actualcoord=[]
    initcoord=start
    coords.append(initcoord)
    actualcoord.append(initcoord)
    rejection_rate=0
    reset=0
    i=0
    previousr=7 #any number over 5 is ok so that it does not interfer with randdir
    reject=0
    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection='3d')
    #plt.ion()
    #plt.show()
    while len(coords)<=iterations:
        stepback=initcoord
        x=initcoord[0]
        y=initcoord[1]
        z=initcoord[2]
    #        r=randdir(kuhnlist[i])
        r=gridpol8.randdir(kuhnlist[len(coords)-1])
        if r[1]!=-previousr:
            previousr=r[1]
            rx=r[0][0]
            ry=r[0][1]
            rz=r[0][2]
            initcoordnext=(x+rx,y+ry,z+rz)
            d=gridpol8.dist3(initcoordnext,center)
    #        print initcoord
            if initcoordnext not in coords:
                if initcoordnext not in occupied:
                    if d<=radius:
                        coords.append(initcoordnext)
                        initcoord=initcoordnext
    #                    i+=1
#                        print len(coords)
                    elif d>radius:
#                        print "out of the nucleus"
                        reject+=1
                elif initcoordnext in occupied:
#                    print "already occupied"
                    reject+=1
            elif initcoordnext in coords:
#                print "going backward", initcoordnext
                reject+=1
            if reject>=100:
                if reset<20:
                    print "reset"
                    reset+=1
                    del coords[-20:]
                    initcoord=coords[-1]
                    reject=0
                if reset>20:
#                    print "could not converge"
                    break
    print "tried ",reset+1," time. You're welcome"
    return(coords,actualcoord,True)

klist=[]
rsegcomp=[]
rkrocomp=[]
lsegcomp=[]
lkrocomp=[]
while False in krstatus or False in klstatus:
    krstatus=[]
    klstatus=[]
    occupied=[]
    for centro in range(len(centromeres)):
        kr=gridpol8.gridpol4(centromeres[centro],(0,0,0),occupied,len(rchrseg[centro]),rchrseg[centro],kuhn,radius,-1000)
        occupied.extend(kr[0])
        rsegcomp.append(len(rchrseg[centro]))
        rkrocomp.append(len(kr[0]))
        print len(occupied)
        len
        kl=gridpol8.gridpol4(centromeres[centro],(0,0,0),occupied,len(lchrseg[centro]),lchrseg[centro],kuhn,radius,-1000)
        occupied.extend(kl[0])
        lsegcomp.append(len(lchrseg[centro]))
        lkrocomp.append(len(kl[0]))
        print len(occupied)
        krstatus.append(kr[2])
        klstatus.append(kl[2])
        kl[0].reverse()
        kl[0].remove(kl[0][-1])
        k=kl[0]+kr[0]
        klist.append(k)

print "l vs s L",lsegcomp,lkrocomp
print "l vs s R",rsegcomp,rkrocomp


##tries=0
###klist=[]
##while False in krstatus or False in klstatus:
###    print "number of tries",tries
##    krstatus=[]
##    klstatus=[]
##    klist=[]
##    occupied=[]
##    occupied1=[]
##    occupied2=[]
##    occupied.extend(centromeres)
###    print len(occupied)
##    for centro in range(1):#in range(len(centromeres)):
##        kr=gridpol(centromeres[centro],(0,0,0),occupied,len(rchrseg[centro]),rchrseg[centro],kuhn,radius,-1000)
##        occupied1.extend(kr[0])
##        occupied.extend(kr[0])
##        krstatus.append(kr[2])
##        kl=gridpol(centromeres[centro],(0,0,0),occupied,len(lchrseg[centro]),lchrseg[centro],kuhn,radius,-1000)
##        kl[0].reverse()
##        kl[0].remove(kl[0][-1])
##        occupied2.extend(kl[0])
##        occupied.extend(kl[0])
##        klstatus.append(kl[2])
##        k=kl[0]+kr[0]
###        k=kr[0]
##        klist.append(k)
###        print centro
##        print centro,len(klist[centro])
##    tries+=1
##

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

centromeres=np.array(centromeres)
cx=centromeres[:,0]
cy=centromeres[:,1]
cz=centromeres[:,2]

pu_centromeres=np.array(pu_centromeres)
pcx=pu_centromeres[:,0]
pcy=pu_centromeres[:,1]
pcz=pu_centromeres[:,2]

ax.plot(cx,cy,cz,color='0')
ax.scatter(cx,cy,cz,color='b',s=50)
ax.scatter(pcx,pcy,pcz,color='0',s=10)


dirveclist=[]
segposlist=[]
for k in range(len(klist)):
    klist[k]=np.array(klist[k])
    u1=klist[k][:,0]
    v1=klist[k][:,1]
    w1=klist[k][:,2]
    dirveclist.append(gridpol8.dirvec(u1,v1,w1))
    segposlist.append(gridpol8.midpoint(u1,v1,w1))
#    ax.plot(u1,v1,w1,color=str(float(k)/len(klist)))
#    ax.plot(u1,v1,w1,color=(random.random(),random.random(),random.random()))
    ax.plot(u1,v1,w1,color='k')
#    ax.scatter(u1,v1,w1,facecolors='none',color='r',s=20)
    if k==2:
        ax.scatter(u1[0:150],v1[0:150],w1[0:150],facecolors='none',color='r',s=20)
        ax.scatter(u1[-150:-2],v1[-150:-2],w1[-150:-2],facecolors='none',color='g',s=20)
    plt.draw()

u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]

xs=np.cos(u)*np.sin(v)
ys=np.sin(u)*np.sin(v)
zs=np.cos(v)

ax.plot_wireframe(xs*radius, ys*radius, zs*radius, color="r")
plt.draw()
plt.show()

initcoord=file("/Volumes/Hua10TB/projets-calcul_backup/repository/simunoyau/initcoords",'w')
initjoint=file("/Volumes/Hua10TB/projets-calcul_backup/repository/simunoyau/initjoints",'w')

print 'len(chrseg[0])',len(chrseg[0])
print 'len(segposlist[0])',len(segposlist[0])
print 'len(klist[0])',len(klist[0])

for k in range(len(chrseg)):
    print len(segposlist[k])
    for seg in range(len(segposlist[k])):
        x=segposlist[k][seg][0]*1e-9
        y=segposlist[k][seg][1]*1e-9
        z=segposlist[k][seg][2]*1e-9
        if chrseg[k][seg]==kuhn:
            sizex=segradius
            sizey=segradius
            sizez=kuhn
        if chrseg[k][seg]==rkuhn:
            sizex=nucradius
            sizey=nucradius
            sizez=nucradius/100
        if dirveclist[k][seg][0]==1:
            mat=gridpol8.rotation_matrix((1,0,0),math.pi/2,'r')
        if dirveclist[k][seg][0]==-1:
            mat=gridpol8.rotation_matrix((1,0,0),math.pi/-2,'r')
        if dirveclist[k][seg][1]==1:
            mat=gridpol8.rotation_matrix((0,1,0),math.pi/2,'r')
        if dirveclist[k][seg][1]==-1:
            mat=gridpol8.rotation_matrix((0,1,0),math.pi/-2,'r')
        if dirveclist[k][seg][2]==1:
            mat=gridpol8.rotation_matrix((0,0,1),math.pi/2,'r')
        if dirveclist[k][seg][2]==-1:
            mat=gridpol8.rotation_matrix((0,0,1),math.pi/-2,'r')
        initcoord.write("k"+str(k+1)+"."+str("%0.3d"%seg)+"\t"+"step 0.0"+"\t"+
        str("%.8e"%x)+"\t"+str("%.8e"%y)+"\t"+str("%.8e"%z)+"\t"+
        str("%.4f"%mat[0][0])+"\t"+str("%.4f"%mat[0][1])+"\t"+str("%.4f"%mat[0][2])+"\t"+
        str("%.4f"%mat[1][0])+"\t"+str("%.4f"%mat[1][1])+"\t"+str("%.4f"%mat[1][2])+"\t"+
        str("%.4f"%mat[2][0])+"\t"+str("%.4f"%mat[2][1])+"\t"+str("%.4f"%mat[2][2])+"\t"+
        str("%.1f"%0)+"\t"+str("%.1f"%0)+"\t"+str("%.1f"%0)+"\t"+
#        str(float(0))+"\t"+str(float(0))+"\t"+str(float(0))+"\t"+
        str("%0.3f"%(sizex/10))+"\t"+str("%0.3f"%(sizey/10))+"\t"+str("%0.3f"%(sizez/10))+
        "\n"
        )
    count=0
    for joint in klist[k]:
        x=joint[0]*1e-9
        y=joint[1]*1e-9
        z=joint[2]*1e-9
        initjoint.write("k"+str(k+1)+"."+str("%0.3d"%count)+"\t"+
                        str("%0.3e"%x)+"\t"+
                        str("%0.3e"%y)+"\t"+
                        str("%0.3e"%z)+"\t"+
                        "\n"
                        )
        count+=1
    initjoint.write("MT")

        
initcoord.close()
initjoint.close()
