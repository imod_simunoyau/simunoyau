#    <This is a script that model nuclear interactions as systems of rigid bodies>
#    Copyright (C) 2008  Hua WONG <wong.hua____at__gmail__dot_com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
import string
import bpy
import mathutils
import math

coords=open('/Volumes/simunoyau/toto/chro1_80.xyz','r')
#coords=open('/Volumes/simunoyau/toto/toto.xyz','r')

xyz=coords.readlines()

scn=bpy.context.scene

for i in range(4):#len(xyz)):
    print(xyz[i])
    varxyz=xyz[i].split(',')
    if varxyz[0]=='NaN':
        pass
    else:
        x=float(varxyz[0])*1e6
        y=float(varxyz[1])*1e6
        z=float(varxyz[2])*1e6
        aa=float(varxyz[3])
        ab=float(varxyz[4])
        ac=float(varxyz[5])
        ba=float(varxyz[6])
        bb=float(varxyz[7])
        bc=float(varxyz[8])
        ca=float(varxyz[9])
        cb=float(varxyz[10])
        cc=float(varxyz[11])                        
    
    mat = mathutils.Matrix()
    mat[0][0:4] = aa, ab, ac, 0
    mat[2][0:4] = ba, bb, bc, 0
    mat[1][0:4] = ca, cb, cc, 0
    mat[3][0:4] = 0 , 0 , 0 , 1

    mat_toggle_YZ=mathutils.Matrix()
    mat_toggle_YZ[0][0:4]=1,0,0,0
    mat_toggle_YZ[0][0:4]=0,0,1,0
    mat_toggle_YZ[0][0:4]=0,1,0,0
    mat_toggle_YZ[0][0:4]=0,0,0,1

    mat=mat_toggle_YZ*mat*mat_toggle_YZ
    
#Matrix mToggle_YZ = new Matrix(
#{1, 0, 0, 0}
#{0, 0, 1, 0}
#{0, 1, 0, 0}
#{0, 0, 0, 1})

    print(mat)
#    mat.transpose()
    eulrot=mat.to_euler()
    print(eulrot)
    
#    bpy.ops.mesh.primitive_cylinder_add(vertices=6,radius=0.01,depth=0.06)
    bpy.ops.mesh.primitive_cylinder_add(vertices=6,radius=0.01,depth=0.06,location=((x,z,y)),rotation=(eulrot[0],eulrot[1],eulrot[2]))
    ob=scn.objects.active
#    mat=mathutils.Matrix.transpose(mat)
#    print(mat)
#    ob.matrix_world=mat

#    mat_rotx = mathutils.Matrix.Rotation(math.radians(-90), 4, mat[0][0:3])
##    mat_roty = mathutils.Matrix.Rotation(math.radians(90), 4, mat[2][0:3])
#
#    mat=mat_rotx*mat
##    mat=mat_roty*mat
#
##    mat[0][0:3] = aa, ba, ca
##    mat[1][0:3] = ab, bb, cb
##    mat[2][0:3] = ac, bc, cc
#
#    #a=mat.to_quaternion()
#    b=mat.to_euler()
#
#    bpy.ops.mesh.primitive_cylinder_add(vertices=6,radius=0.01,depth=0.06)
#    ob=scn.objects.active
#    ob.matrix_world=mat
#    ob.location=(x,y,z)
#    
##    bpy.ops.mesh.primitive_cylinder_add(vertices=6,radius=0.01,depth=0.06,location=(x,y,z),rotation=b)
#    
##    bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=4,size=0.03,location=(x,y,z))
##    ob.matrix_world=mat
##bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=4,size=1)