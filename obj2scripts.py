#    <This is a script that model nuclear interactions as systems of rigid bodies>
#    Copyright (C) 2008  Hua WONG <wong.hua____at__gmail__dot_com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
import string

def obj2cw(wavefront_obj):
	cellwallobj=file(wavefront_obj,'r')
	#cellwallobj=file('cellwalltop.obj','r')
	#cellwallpy=file('cellwalltop.cw','w')
	cellwalldat=cellwallobj.readlines()
	vertlist=[]
	facelist=[]
	nuc1=[]
	for lines in cellwalldat:
		if lines[0]=='v':
			linecontent=string.split(lines)
			vertlist.append(linecontent)
		elif lines[0]=='f':
			linecontent=string.split(lines)
			facelist.append(linecontent)
	for el in vertlist:
		el.remove(el[0])
		el[0]=float(el[0])
		el[1]=float(el[1])
		el[2]=float(el[2])
	for el in facelist:
        	el.remove(el[0])
	        el[0]=float(el[0])
        	el[1]=float(el[1])
	        el[2]=float(el[2])
	return(vertlist,facelist)

